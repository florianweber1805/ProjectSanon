﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Controls : MonoBehaviour {

	/// <summary>
	/// Get own instance and return.
	/// </summary>
	private static Controls instance;
	public static Controls Control {
		get {
			if (instance == null) {
				instance = FindObjectOfType<Controls>();
			}
			return instance;
		}
	}

	bool playerIndexSet = false;
	PlayerIndex playerIndex;
	GamePadState state;
	GamePadState prevState;

	/// <summary>
	/// Initialize gamepad controls.
	/// </summary>
	void Start() {
		DontDestroyOnLoad(gameObject);
	}

	/// <summary>
	/// Main update cycle.
	/// Find gamepads.
	/// </summary>
	void Update () {

		// Find a PlayerIndex, for a single player game
		// Will find the first controller that is connected ans use it
		if (!playerIndexSet || !prevState.IsConnected)
		{
			for (int i = 0; i < 4; ++i)
			{
				PlayerIndex testPlayerIndex = (PlayerIndex)i;
				GamePadState testState = GamePad.GetState(testPlayerIndex);
				if (testState.IsConnected)
				{
					Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
					playerIndex = testPlayerIndex;
					playerIndexSet = true;
				}
			}
		}
		
		prevState = state;
		state = GamePad.GetState(playerIndex);

	}

	/// <summary>
	/// Gets state of A Button.
	/// </summary>
	public bool GetADown() { return prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed; }
	public bool GetAUp() { return prevState.Buttons.A == ButtonState.Pressed && state.Buttons.A == ButtonState.Released; }
	public bool GetA() { return state.Buttons.A == ButtonState.Pressed; }

	/// <summary>
	/// Gets state of B Button.
	/// </summary>
	public bool GetBDown() { return prevState.Buttons.B == ButtonState.Released && state.Buttons.B == ButtonState.Pressed; }
	public bool GetBUp() { return prevState.Buttons.B == ButtonState.Pressed && state.Buttons.B == ButtonState.Released; }
	public bool GetB() { return state.Buttons.B == ButtonState.Pressed; }

	/// <summary>
	/// Gets state of X Button.
	/// </summary>
	public bool GetXDown() { return prevState.Buttons.X == ButtonState.Released && state.Buttons.X == ButtonState.Pressed; }
	public bool GetXUp() { return prevState.Buttons.X == ButtonState.Pressed && state.Buttons.X == ButtonState.Released; }
	public bool GetX() { return state.Buttons.X == ButtonState.Pressed; }

	/// <summary>
	/// Gets state of Y Button.
	/// </summary>
	public bool GetYDown() { return prevState.Buttons.Y == ButtonState.Released && state.Buttons.Y == ButtonState.Pressed; }
	public bool GetYUp() { return prevState.Buttons.Y == ButtonState.Pressed && state.Buttons.Y == ButtonState.Released; }
	public bool GetY() { return state.Buttons.Y == ButtonState.Pressed; }

	/// <summary>
	/// Gets state of Start Button.
	/// </summary>
	public bool GetStartDown() { return prevState.Buttons.Start == ButtonState.Released && state.Buttons.Start == ButtonState.Pressed; }
	public bool GetStartUp() { return prevState.Buttons.Start == ButtonState.Pressed && state.Buttons.Start == ButtonState.Released; }
	public bool GetStart() { return state.Buttons.Start == ButtonState.Pressed; }

    /// <summary>
    /// Get state of Back Button
    /// </summary>
    /// <returns></returns>
    public bool GetBackDown() { return prevState.Buttons.Back == ButtonState.Released && state.Buttons.Back == ButtonState.Pressed; }
    public bool GetBackUp() { return prevState.Buttons.Back == ButtonState.Pressed && state.Buttons.Back == ButtonState.Released; }
    public bool GetBack() { return state.Buttons.Back == ButtonState.Pressed; }

    /// <summary>
    /// Gets state of Trigger Buttons.
    /// </summary>
    public float GetLT() { return state.Triggers.Left; }
	public float GetRT() { return state.Triggers.Right; }

	/// <summary>
	/// Gets state of Left Stick.
	/// </summary>
	public float GetLV() { return state.ThumbSticks.Left.Y; }
	public float GetLH() { return state.ThumbSticks.Left.X; }
	public bool GetLDown() { return prevState.Buttons.LeftStick == ButtonState.Released && state.Buttons.LeftStick == ButtonState.Pressed; }
	public bool GetLUp() { return prevState.Buttons.LeftStick == ButtonState.Pressed && state.Buttons.LeftStick == ButtonState.Released; }
	public bool GetL() { return state.Buttons.LeftStick == ButtonState.Pressed; }

	/// <summary>
	/// Gets state of Right Stick.
	/// </summary>
	public float GetRV() { return state.ThumbSticks.Right.Y; }
	public float GetRH() { return state.ThumbSticks.Right.X; }
	public bool GetRDown() { return prevState.Buttons.RightStick == ButtonState.Released && state.Buttons.RightStick == ButtonState.Pressed; }
	public bool GetRUp() { return prevState.Buttons.RightStick == ButtonState.Pressed && state.Buttons.RightStick == ButtonState.Released; }
	public bool GetR() { return state.Buttons.RightStick == ButtonState.Pressed; }

	/// <summary>
	/// Gets the LB down.
	/// </summary>
	/// <returns><c>true</c>, if LB down was gotten, <c>false</c> otherwise.</returns>
	public bool GetLBDown() { return prevState.Buttons.LeftShoulder == ButtonState.Released && state.Buttons.LeftShoulder == ButtonState.Pressed; }
	public bool GetLBUp() { return prevState.Buttons.LeftShoulder == ButtonState.Pressed && state.Buttons.LeftShoulder == ButtonState.Released; }
	public bool GetLB() { return state.Buttons.LeftShoulder == ButtonState.Pressed; }

	/// <summary>
	/// Gets the RB down.
	/// </summary>
	/// <returns><c>true</c>, if RB down was gotten, <c>false</c> otherwise.</returns>
	public bool GetRBDown() { return prevState.Buttons.RightShoulder == ButtonState.Released && state.Buttons.RightShoulder == ButtonState.Pressed; }
	public bool GetRBUp() { return prevState.Buttons.RightShoulder == ButtonState.Pressed && state.Buttons.RightShoulder == ButtonState.Released; }
	public bool GetRB() { return state.Buttons.RightShoulder == ButtonState.Pressed; }

	/// <summary>
	/// Vibrate the specified LStrength and RStrength.
	/// </summary>
	/// <param name="LStrength">L strength.</param>
	/// <param name="RStrength">R strength.</param>
	public IEnumerator Vibrate(float LStrength, float RStrength, float Length = 1f) {
		GamePad.SetVibration(playerIndex, LStrength, RStrength);
		yield return new WaitForSeconds(Length);
		GamePad.SetVibration(playerIndex, 0f, 0f);
	}

	void OnApplicationQuit() {
		GamePad.SetVibration(playerIndex, 0f, 0f);
	}

}
