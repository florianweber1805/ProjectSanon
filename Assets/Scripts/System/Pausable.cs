﻿using UnityEngine;
using System.Collections;

public class Pausable : MonoBehaviour {

    private bool paused;
    public bool Paused
    {
        get
        {
            return paused;
        }
    }

	// Use this for initialization
	public virtual void Start () {
        Pausing.PauseGame += Pause;
        Pausing.ResumeGame += Resume;
    }

    void Pause()
    {
        paused = true;
    }

    void Resume()
    {
        paused = false;
    }
}
