﻿using UnityEngine;
using System.Collections;

namespace Helper
{

    #region Cam

    public class Cam
    {
        public static Camera CurrentCamera
        {
            get
            {
                Camera cam = null;
                if (Camera.main)
                {
                    cam = Camera.main;
                }
                else
                {
                    cam = GameObject.FindGameObjectWithTag(Tags.PlayerCamera).GetComponent<Camera>();
                }
                return cam;
            }            
        }
    }

    #endregion

    #region Reference Cash

    public class Settings
    {

        public static T Iif<T>(bool cond, T left, T right)
        {
            return cond ? left : right;
        }

        /// <summary>
        /// Text Speed
        /// </summary>
        private static string text_speed = "TextSpeed";
        public static float TextSpeed
        {
            get { return PlayerPrefs.GetFloat(text_speed, 0.0055f); }
            set { PlayerPrefs.SetFloat(text_speed, value); }
        }

        /// <summary>
        /// Third Person Camera
        /// </summary>
        private static string camera_invert_x = "CameraInvertX";
        public static bool CameraInvertX
        {
            get { return PlayerPrefs.GetInt(camera_invert_x, 0) == 1; }
            set { PlayerPrefs.SetInt(camera_invert_x, Iif<int>(value, 1, 0)); }
        }

        private static string camera_invert_y = "CameraInvertY";
        public static bool CameraInvertY
        {
            get { return PlayerPrefs.GetInt(camera_invert_y, 1) == 1; }
            set { PlayerPrefs.SetInt(camera_invert_y, Iif<int>(value, 1, 0)); }
        }

        private static string camera_sensitivity = "CameraSensitivity";
        public static float CameraSensitivity
        {
            get { return PlayerPrefs.GetFloat(camera_sensitivity, 0.5f); }
            set { PlayerPrefs.SetFloat(camera_sensitivity, value); }
        }

        /// <summary>
        /// First person camera
        /// </summary>
        private static string camera_fp_invert_x = "CameraFPInvertX";
        public static bool CameraFPInvertX
        {
            get { return PlayerPrefs.GetInt(camera_fp_invert_x, 0) == 1; }
            set { PlayerPrefs.SetInt(camera_fp_invert_x, Iif<int>(value, 1, 0)); }
        }

        private static string camera_fp_invert_y = "CameraFPInvertY";
        public static bool CameraFPInvertY
        {
            get { return PlayerPrefs.GetInt(camera_fp_invert_y, 0) == 1; }
            set { PlayerPrefs.SetInt(camera_fp_invert_y, Iif<int>(value, 1, 0)); }
        }

        private static string camera_fp_sensitivity = "CameraFPSensitivity";
        public static float CameraFPSensitivity
        {
            get { return PlayerPrefs.GetFloat(camera_fp_sensitivity, 0.5f); }
            set { PlayerPrefs.SetFloat(camera_fp_sensitivity, value); }
        }

        /// <summary>
        /// Volume
        /// </summary>
        private static string volume_master = "VolumeMaster";
        public static float VolumeMaster
        {
            get { return PlayerPrefs.GetFloat(volume_master, 0.5f); }
            set { PlayerPrefs.SetFloat(volume_master, value); }
        }

        private static string volume_sounds = "VolumeSounds";
        public static float VolumeSounds
        {
            get { return PlayerPrefs.GetFloat(volume_sounds, 1f); }
            set { PlayerPrefs.SetFloat(volume_sounds, value); }
        }
        public static float SoundVolume
        {
            get {
                return PlayerPrefs.GetFloat(volume_sounds, 1f) * PlayerPrefs.GetFloat(volume_master, 0.5f);
            }
        }

        private static string volume_music = "VolumeMusic";
        public static float VolumeMusic
        {
            get { return PlayerPrefs.GetFloat(volume_music, 1f); }
            set { PlayerPrefs.SetFloat(volume_music, value); }
        }
        public static float MusicVolume
        {
            get
            {
                return PlayerPrefs.GetFloat(volume_music, 1f) * PlayerPrefs.GetFloat(volume_master, 0.5f);
            }
        }

        /// <summary>
        /// Save settings
        /// </summary>
        public static void Save()
        {
            PlayerPrefs.Save();
        }

    }

    public class Tags
    {

        public static string Untagged = "Untagged";
        public static string Respawn = "Respawn";
        public static string Finish = "Finish";
        public static string EditorOnly = "EditorOnly";
        public static string MainCamera = "MainCamera";
        public static string Player = "Player";
        public static string GameController = "GameController";

        public static string PlayerCamera = "PlayerCamera";

    }

    public class Layers
    {
        public static string Geometry = "Geometry";
        public static string Player = "Player";
        public static string EdgeJump = "EdgeJump";
    }

    public class Scenes
    {
        public static string MainMenu = "menu_main";
        public static string TestEnvironment = "test2";
    }

    public class Resource
    {

        public static string AnimatorController = "Characters/player_controller";

    }

    public class AnimatorConditions
    {

        // Movement
        public static string Speed = "Speed";
        public static string Direction = "Direction";
        public static string Grounded = "Grounded";
        public static string AirVelocity = "AirVelocity";
        public static string Roll = "Roll";
        public static string CanWalk = "CanWalk";

        // Focus
        public static string Focus = "Focus";
        public static string Dodge = "Dodge";

        // Jumping
        public static string Jump = "Jump";
        public static string JumpEdge = "JumpEdge";
        public static string JumpDeath = "JumpToDeath";        

        // Climbing
        public static string Climb = "Climb";
        public static string Hang = "Hang";
        public static string Drop = "Drop";

        // Draw Weapon
        public static string DrawWeapon = "DrawWeapon";
        public static string SheathWeapon = "SheathWeapon";
        public static string WeaponIsOut = "WeaponIsOut";

        // Attacking
        public static string SwordStrikeHorizontalCount = "SwordStrikeHorizontalCount";
        public static string SwordStrikeHorizontal = "SwordStrikeHorizontal";
        public static string SwordRepel = "SwordRepel";

    }

    public class AnimationStates
    {

        // Movement
        public static string Roll = "Roll";

        // Climbing
        public static string Climb = "Climb";
        public static string Hang = "Hang";

        // Focus
        public static string Dodge = "Dodge";

        // Draw Weapon
        public static string DrawWeapon = "DrawWeapon";
        public static string SheathWeapon = "SheathWeapon";

        // Jumping
        public static string Jump = "Jump";
        public static string JumpEdge = "JumpEdge";
        public static string JumpDeath = "JumpToDeath";
        public static string Fall = "Fall";

        // Attacking
        public static string SwordStrikeHorizontal = "SwordStrikeHorizontal";
        public static string SwordRepelHorizontal = "SwordRepelHorizontal";

    }

    #endregion

    #region FSM Enumerations

    public enum CameraState { Normal, Target, FirstPerson, Speaking };

    #endregion

    #region Object Structures

    public class CameraObject
    {

        private Vector3 position;
        private Transform xForm;

        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        public Transform XForm
        {
            get { return xForm; }
            set { xForm = value; }
        }

        public CameraObject(string camName, Vector3 pos, Transform transform, Transform parent, Vector3 rotate)
        {
            position = pos;
            xForm = transform;
            xForm.name = camName;
            xForm.position = pos;
            xForm.Rotate(rotate);
            xForm.parent = parent;
            xForm.localPosition = position;
        }

    }

    #endregion

}
