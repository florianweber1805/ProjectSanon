﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(FPSCounter))]
[RequireComponent(typeof(Controls))]
public class SceneStarter : MonoBehaviour {

    [SerializeField]
    private Vector3 StartPosition;
    [SerializeField]
    private GameObject Player;
    [SerializeField]
    private GameObject InGameMenu;

	void Start () {
        GameObject m = (GameObject)Instantiate<GameObject>(InGameMenu);
        m.name = "InGame Menu";
        GameObject p = (GameObject)Instantiate<GameObject>(Player);
        p.transform.position = StartPosition;
        p.name = "Player";
	}

}
