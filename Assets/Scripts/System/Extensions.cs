﻿using UnityEngine;

public static class Extensions {

    // ####################################################################################################################################### Animator

    public static int GetDominantLayer(this Animator Animator) {
		int dominant_index = 0;
		float dominant_weight = 0f;
		float weight = 0f;
		for (int index = 0; index < Animator.layerCount; index++) {
			weight = Animator.GetLayerWeight(index);
			if (weight > dominant_weight) {
				dominant_weight = weight;
				dominant_index = index;
			}
		}
		return dominant_index;
	}

	public static void SetTrigger(this Animator Animator, string name, bool resetOthers, bool checkTriggered = true) {
		if (resetOthers) {
			foreach (AnimatorControllerParameter parameter in Animator.parameters) {
				if (parameter.type == AnimatorControllerParameterType.Trigger) {
					if (parameter.name != name) {
						Animator.ResetTrigger(parameter.name);
					}
				}
			}
		}
		if (!checkTriggered || !Animator.GetBool(name)) Animator.SetTrigger(name);
	}

    // ####################################################################################################################################### GameObject

    public static bool HasConversation(this GameObject GameObject)
    {
        if (GameObject)
        {
            Conversation conv = GameObject.GetComponent<Conversation>();
            if (conv) return true;
        }        
        return false;
    }

    public static Conversation Conversation(this GameObject GameObject)
    {
        Conversation conv = GameObject.GetComponent<Conversation>();
        return conv;
    }

    // ####################################################################################################################################### Vector3

    public static Vector3 Direction(this Vector3 From, Vector3 To)
    {
        Vector3 dir = To - From;
        return dir.normalized;
    }

}

///////////////////////////////////////////////////////////////
/// <summary>
/// Game object tools.
/// </summary>
///////////////////////////////////////////////////////////////
static public class GameObjectTools
{
    ///////////////////////////////////////////////////////////
    // Essentially a reimplementation of 
    // GameObject.GetComponentInChildren< T >()
    // Major difference is that this DOES NOT skip deactivated 
    // game objects
    ///////////////////////////////////////////////////////////
    static public TType GetComponentInChildren<TType>(GameObject objRoot) where TType : Component
    {
        // if we don't find the component in this object 
        // recursively iterate children until we do
        TType tRetComponent = objRoot.GetComponent<TType>();

        if (null == tRetComponent)
        {
            // transform is what makes the hierarchy of GameObjects, so 
            // need to access it to iterate children
            Transform trnsRoot = objRoot.transform;
            int iNumChildren = trnsRoot.childCount;

            // could have used foreach(), but it causes GC churn
            for (int iChild = 0; iChild < iNumChildren; ++iChild)
            {
                // recursive call to this function for each child
                // break out of the loop and return as soon as we find 
                // a component of the specified type
                tRetComponent = GetComponentInChildren<TType>(trnsRoot.GetChild(iChild).gameObject);
                if (null != tRetComponent)
                {
                    break;
                }
            }
        }

        return tRetComponent;
    }
}