﻿using UnityEngine;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Mono.Data.Sqlite;

public class Data {

    private const string conn = "URI=file:%PATH%/sanon_data.db3";
    private static IDbConnection db;

    public static void OpenDB()
    {
        db = new SqliteConnection(conn.Replace("%PATH%", Application.dataPath));
        db.Open();
    }

    public static void CloseDB()
    {
        db.Close();
    }

    public static List<ConversationPiece> GetConversation(int Character, int State, int Option)
    {
        if (db.State != ConnectionState.Open) OpenDB();
        SqliteCommand cmd = (SqliteCommand)db.CreateCommand();
        cmd.CommandText = "SELECT text, size, speed FROM conversation WHERE character = @character AND state = @state AND option = @option ORDER BY piece ASC;";
        cmd.Parameters.Add(new SqliteParameter("character", Character));
        cmd.Parameters.Add(new SqliteParameter("state", State));
        cmd.Parameters.Add(new SqliteParameter("option", Option));
        SqliteDataReader reader = cmd.ExecuteReader();
        List<ConversationPiece> text = new List<ConversationPiece>();
        string str;
        int size;
        float speed;
        // Read data
        while (reader.Read())
        {
            str = reader.GetString(0).Replace("%n%", Environment.NewLine);
            size = reader.GetInt32(1);
            speed = reader.GetFloat(2);
            ConversationPiece cp = new ConversationPiece(str, (ConversationPiece.ConversationFontSize)size, speed);
            text.Add(cp);
        }
        // Close
        reader.Close();
        cmd.Dispose();
        // Return
        return text;
    }

}
