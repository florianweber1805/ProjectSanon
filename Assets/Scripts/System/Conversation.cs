﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConversationPiece
{
    public enum ConversationFontSize { Normal = 2, Small = 1, Big = 3 };

    private string text;
    private ConversationFontSize size;
    private float speed;

    public string Text
    {
        get
        {
            return text;
        }
    }

    public ConversationFontSize Size
    {
        get
        {
            return size;
        }
    }

    public float Speed
    {
        get
        {
            return speed;
        }
    }

    public ConversationPiece(string Text, ConversationFontSize Size, float Speed)
    {
        text = Text;
        size = Size;
        speed = Speed;
    }
}

public class Conversation : MonoBehaviour {

    public GameObject BaseModel;

    private int state = 1;
    public int Character = 1;

    public List<ConversationPiece> Speak(int Option = 1)
    {
        List<ConversationPiece> text = Data.GetConversation(Character, state, Option);
        return text;
    }

}
