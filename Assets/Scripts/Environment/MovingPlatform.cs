﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : Pausable {

    public enum MoveType { Repeat, Reverse };

    [System.Serializable]
    public class EditorTransform
    {
        public Vector3 position;
        public Vector3 rotation;
    }

    public bool Active;
    public MoveType Type;
    [SerializeField]
    private EditorTransform StartTransform;
    [SerializeField]
    private EditorTransform[] Transforms;
    [SerializeField]
    private int TransformIndex = 1;
    [SerializeField]
    private float Speed = 1f;
    [SerializeField]
    private float SpeedDivider = 10f;

    // Position
    private EditorTransform last_transform = new EditorTransform();
    private EditorTransform current_transform = new EditorTransform();

    private float lerp;
    private bool direction;

    public override void Start()
    {
        base.Start();
        current_transform.position = StartTransform.position;
        current_transform.rotation = StartTransform.rotation;
        last_transform.position = current_transform.position;
        last_transform.rotation = current_transform.rotation;
    }

	void Update () {
        if (!Paused && Active)
        {
            lerp += (Speed / SpeedDivider) * Time.deltaTime;
            current_transform.position = Vector3.Lerp(last_transform.position, Transforms[TransformIndex].position, lerp);
            current_transform.rotation = Vector3.Lerp(last_transform.rotation, Transforms[TransformIndex].rotation, lerp);
            if (lerp >= 1f)
            {
                switch (Type)
                {
                    case MoveType.Repeat:
                        Repeat();
                        break;
                    case MoveType.Reverse:
                        Reverse();
                        break;
                }
                last_transform.position = current_transform.position;
                last_transform.rotation = current_transform.rotation;
                lerp = 0f;
            }
            transform.position = current_transform.position;
            transform.rotation = Quaternion.Euler(current_transform.rotation);
        }        
	}

    void Repeat()
    {
        if (TransformIndex < Transforms.Length-1)
        {
            TransformIndex++;
        }
        else
        {
            TransformIndex = 0;
        }
    }

    void Reverse()
    {
        if (direction)
        {
            if (TransformIndex < Transforms.Length-1)
            {
                TransformIndex++;
            }
            else
            {
                direction = !direction;
                Reverse();
            }
        }
        else
        {
            if (TransformIndex > 0)
            {
                TransformIndex--;
            }
            else
            {
                direction = !direction;
                Reverse();
            }
        }
    }

}
