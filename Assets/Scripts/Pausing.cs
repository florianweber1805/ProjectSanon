﻿using UnityEngine;
using System.Collections;

public static class Pausing
{
    public enum PauseType { Game, Enemies };

    private static bool game_paused;

    public delegate void pause_game();
    public static event pause_game PauseGame;

    public delegate void resume_game();
    public static event resume_game ResumeGame;

    public static bool enemies_paused;

    public delegate void pause_enemies();
    public static event pause_enemies PauseEnemies;

    public delegate void resume_enemies();
    public static event resume_enemies ResumeEnemies;

    public static void Toggle(PauseType Type = PauseType.Game)
    {
        switch (Type)
        {
            case PauseType.Game:
                game_paused = !game_paused;
                if (!game_paused) Resume(Type);
                if (game_paused) Pause(Type);
                break;
            case PauseType.Enemies:
                enemies_paused = !enemies_paused;
                if (!enemies_paused) Resume(Type);
                if (enemies_paused) Pause(Type);
                break;
        }
    }

    public static void Pause(PauseType Type = PauseType.Game)
    {
        switch (Type)
        {
            case PauseType.Game:
                PauseGame();
                game_paused = true;
                break;
            case PauseType.Enemies:
                PauseEnemies();
                enemies_paused = true;
                break;
        }
    }

    public static void Resume(PauseType Type = PauseType.Game)
    {
        switch (Type)
        {
            case PauseType.Game:
                ResumeGame();
                game_paused = false;
                break;
            case PauseType.Enemies:
                ResumeEnemies();
                enemies_paused = false;
                break;
        }
    }

    public static bool Paused(PauseType Type = PauseType.Game)
    {
        switch (Type)
        {
            case PauseType.Game:
                return game_paused;
            case PauseType.Enemies:
                return enemies_paused;
        }
        return false;
    }

}
