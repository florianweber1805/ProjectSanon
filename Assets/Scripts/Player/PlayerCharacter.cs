﻿using UnityEngine;
using System.Collections;
using Helper;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(PlayerMotor))]
[RequireComponent(typeof(PlayerCamera))]
[RequireComponent(typeof(PlayerAttack))]
[RequireComponent(typeof(PlayerClimbing))]
[RequireComponent(typeof(PlayerJumping))]
[AddComponentMenu("Player/Character")]
public class PlayerCharacter : MonoBehaviour {

	/// <summary>
	/// Objects.
	/// </summary>
	private Animator animator;
	private Rigidbody body;
	private CapsuleCollider col;
	private RuntimeAnimatorController animator_controller;

	/// <summary>
	/// Get components.
	/// </summary>
	void Awake() {
		animator = GetComponent<Animator>();
		body = GetComponent<Rigidbody>();
		col = GetComponent<CapsuleCollider>();
	}

	/// <summary>
	/// Initialize components.
	/// </summary>
	void Start () {
		// Rigidbody
		body.mass = 100f;
		body.constraints = RigidbodyConstraints.FreezeRotation;
		// Capsule Collider
		col.height = 1.5f;
		col.center = new Vector3(0f, 0.735f, 0f);
		col.radius = 0.35f;
		// Animator
		animator_controller = Resources.Load(Resource.AnimatorController) as RuntimeAnimatorController;
		animator.runtimeAnimatorController = animator_controller;
	}

}
