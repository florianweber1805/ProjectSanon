﻿using UnityEngine;
using System.Collections;
using Helper;

public class PlayerClimbing : MonoBehaviour {

    public enum climb_state { None, Wait, Jump, Hang, Climb, Drop };

    // Objects
    private Transform t;
    private Animator a;
    private PlayerJumping j;
    private PlayerMotor pm;

    // Climb will
    [SerializeField]
    private float ClimbAtWill = 0.3f;
    [SerializeField]
    private float LowerAtWill = 2f;
    private float climb_will;
    private float time_since_collision;
    [SerializeField]
    private float LowerClimbWillAfter = 1f;

    // Climb
    private GameObject climb_target;
    private climb_state state = climb_state.None;
    private bool climb_was_playing;
    private Vector3 climb_position;
    private Vector3 climb_vector;
    private bool placed_for_climb;

    // Detection
    private float wall_detection_length = 1f;
    private float wall_y_offset = 0.3f;
    private float max_climb_height = 3f;
    [SerializeField]
    private float MinimumStepSize = 1f;
    [SerializeField]
    private float MaximumStepSize = 2.5f;
    private float step_from_edge = 0.05f;

    // Jump
    private jump_match jam;
    private jump_match jtm;
    // Jump match class
    private class jump_match
    {
        public Vector3 Position; public Quaternion Rotation; public MatchTargetWeightMask weight; public float From; public float To;
        public jump_match(Vector3 Position, Quaternion Rotation, float From, float To)
        {
            this.Position = Position; this.Rotation = Rotation; this.weight = new MatchTargetWeightMask(Vector3.one, 1f); this.From = From; this.To = To;
        }
    }

    /// <summary>
    /// Get climbing state
    /// </summary>
    public climb_state State { get { return state; } }

    /// <summary>
    /// Check if jump animation is playing
    /// </summary>
    public bool IsJumpPlaying { get { return a.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.JumpEdge); } }

    /// <summary>
    /// Check if player is rolling.
    /// </summary>
    private bool IsClimbPlaying { get { return a.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Climb); } }

    /// <summary>
    /// Check if hang animation is playing
    /// </summary>
    private bool IsHangPlaying { get { return a.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Hang); } }

    /// <summary>
    /// Awake
    /// </summary>
    void Awake()
    {
        a = GetComponent<Animator>();
        j = GetComponent<PlayerJumping>();
        t = transform;
        pm = GetComponent<PlayerMotor>();
    }


    /// <summary>
    /// Main update cycle
    /// </summary>
    void Update()
    {

        switch (state)
        {
            case climb_state.Drop:
                Dropping();
                break;
            case climb_state.Climb:
                Climb();
                break;
            case climb_state.Hang:
                Hanging();
                break;
            case climb_state.Jump:
                Jumping();
                break;
            case climb_state.None:
                //if (climb_will >= ClimbAtWill)
                //{
                //    climb_will = 0f;
                //    ExecuteClimb();
                //}            
                if (time_since_collision > LowerClimbWillAfter) climb_will = 0f;
                else time_since_collision += Time.deltaTime;
                Mathf.Clamp(climb_will, 0f, ClimbAtWill);
                break;
        }
    }

    /// <summary>
    /// Collision
    /// </summary>
    /// <param name="col"></param>
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer(Layers.Geometry) && col.contacts[0].point.y > t.position.y + 0.2f)
        {
            if (j.IsJumping)
            {
                ExecuteClimb(col);
            }
            else
            {
                IncreaseClimbWill(col.gameObject, col.contacts[0].normal);
                time_since_collision = 0f;
            }            
        }
    }

    /// <summary>
    /// Collision stay
    /// </summary>
    /// <param name="col"></param>
    void OnCollisionStay(Collision col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer(Layers.Geometry) && col.contacts[0].point.y > t.position.y + 0.2f)
        {
            IncreaseClimbWill(col.gameObject, col.contacts[0].normal);
            if (climb_will >= ClimbAtWill)
            {
                climb_will = 0f;
                ExecuteClimb(col);
            }
        }
    }

    /// <summary>
    /// Climb will
    /// </summary>
    /// <param name="Target"></param>
    /// <param name="col_vector"></param>
    void IncreaseClimbWill(GameObject Target, Vector3 col_vector)
    {
        if (pm.IsGrounded && climb_target && Target == climb_target)
        {
            if (pm.InputDirection != Vector3.zero && Vector3.Angle(-col_vector, pm.InputDirection) < 45f)
            {
                climb_will += Time.deltaTime;
            }
        }
        else
        {
            climb_target = Target;
            climb_will = 0f;
        }
        
    }

    /// <summary>
    /// Start process to climb
    /// </summary>
    void ExecuteClimb(Collision col)
    {
        if (col.contacts.Length > 0)
        {
            Vector3 rotation = -col.contacts[0].normal;
            rotation.y = 0f;
            //Make a reaycast to get a position to climb up to
            Vector3 start_down = col.contacts[0].point + (Vector3.up * max_climb_height) + (rotation * step_from_edge);
            Vector3 dir_down = -Vector3.up;
            Ray ray_down = new Ray(start_down, dir_down);
            RaycastHit hit_down = new RaycastHit();
            if (Physics.Raycast(ray_down, out hit_down, max_climb_height * 2f, LayerMask.GetMask(Layers.Geometry)))
            {                
                float diff = hit_down.point.y - transform.position.y;
                if (diff < MaximumStepSize && diff > MinimumStepSize && state == climb_state.None && !j.IsJumping)
                {
                    jam = new jump_match(hit_down.point - (Vector3.up * 0.735f), Quaternion.LookRotation(rotation), 0f, 0.8f);
                    jtm = new jump_match(hit_down.point, transform.rotation, 0.8f, 1f);
                    PrepareClimbing(hit_down.point, rotation);
                    state = climb_state.Jump;
                    a.SetTrigger(AnimatorConditions.JumpEdge, true);
                }
                else if (diff < MinimumStepSize || (j.IsJumping && diff < MaximumStepSize))
                {
                    transform.SetParent(col.collider.transform, true);
                    PrepareClimbing(hit_down.point, rotation);
                    StartClimbing();
                }
            }
        }        
    }

    /// <summary>
    /// Lower to platform edge
    /// </summary>
    public void LowerToClimb()
    {
        bool success; Vector3 start; Vector3 dir; Ray ray; RaycastHit hit = new RaycastHit();
        Vector3 start_down; Vector3 dir_down; Ray ray_down; RaycastHit hit_down = new RaycastHit();

        start = transform.position + (transform.forward * (wall_detection_length - 0.1f)) + (-transform.up * wall_y_offset);
        dir = -transform.forward;
        ray = new Ray(start, dir);
        success = Physics.Raycast(ray, out hit, wall_detection_length, LayerMask.GetMask("Geometry"));
        if (success)
        {
            // Make a reaycast to get a position to climb up to
            start_down = hit.point + (hit.transform.up * max_climb_height) + (-hit.normal * step_from_edge);
            dir_down = -hit.transform.up;
            ray_down = new Ray(start_down, dir_down);
            if (Physics.Raycast(ray_down, out hit_down, max_climb_height, LayerMask.GetMask("Geometry")))
            {
                transform.SetParent(hit.collider.gameObject.transform, true);
                PrepareClimbing(hit_down.point, -hit.normal);
                StartClimbing();
            }
        }
    }
    
    /// <summary>
    /// Climb controls
    /// </summary>
    private void Climb()
    {
        if (IsClimbPlaying) climb_was_playing = true;
        if (climb_was_playing && !IsClimbPlaying)
        {
            climb_was_playing = false;
            state = climb_state.None;
            a.SetBool(AnimatorConditions.CanWalk, true);
        }
    }

    /// <summary>
    /// Apply jump match
    /// </summary>
    private void Jumping()
    {
        if (IsJumpPlaying)
        {
            if (!a.IsInTransition(a.GetDominantLayer()))
            {
                if (jam != null) a.MatchTarget(jam.Position, jam.Rotation, AvatarTarget.Root, jam.weight, jam.From, jam.To);
                if (jtm != null) a.MatchTarget(jtm.Position, jtm.Rotation, AvatarTarget.Root, jtm.weight, jtm.From, jtm.To);
            }
            else
            {
                StartClimbing();
            }
        }        
    }

    /// <summary>
    /// Drop controls
    /// </summary>
    private void Dropping()
    {
        if (pm.IsGrounded)
        {
            state = climb_state.None;
            a.SetBool(AnimatorConditions.CanWalk, true);
        }
    }

    /// <summary>
    /// Climbing mechanic.
    /// Input is needed to switch from hang to to climb state.
    /// </summary>
    private void Hanging()
    {
        float diff = Vector3.Angle(pm.InputDirection, climb_vector);
        if (diff < 30f)
        {
            if (climb_will >= ClimbAtWill)
            {
                state = climb_state.Climb;
                a.SetTrigger(AnimatorConditions.Climb, true);
                climb_will = 0f;
            }
            else
            {
                if (IsHangPlaying && !a.IsInTransition(a.GetDominantLayer())) climb_will += Time.deltaTime;
            }
        }
        else if (diff > 135f)
        {
            if (climb_will >= LowerAtWill)
            {
                state = climb_state.Drop;
                transform.position = transform.position + (-transform.forward * 0.5f) + (-transform.up * 0.735f);
                a.SetTrigger(AnimatorConditions.Drop, true);
                climb_will = 0f;
            }
            else
            {
                if (IsHangPlaying && !a.IsInTransition(a.GetDominantLayer())) climb_will += Time.deltaTime;
            }
        }
        else
        {
            climb_will -= Time.deltaTime;            
        }
        climb_will = Mathf.Clamp(climb_will, 0f, LowerAtWill);
    }

    

    /// <summary>
    /// Start climbing trigger.
    /// </summary>
    private void StartClimbing()
    {
        a.SetBool(AnimatorConditions.CanWalk, false);
        a.SetTrigger(AnimatorConditions.Hang, true);
        state = climb_state.Hang;
        StartCoroutine(SetClimbPosition());
    }

    /// <summary>
    /// Set climb position
    /// </summary>
    /// <returns></returns>
    private IEnumerator SetClimbPosition()
    {
        yield return new WaitForFixedUpdate();
        transform.forward = climb_vector;
        transform.position = climb_position;        
    }

    /// <summary>
    /// Prepare climbing trigger with data.
    /// </summary>
    private void PrepareClimbing(Vector3 Position, Vector3 Vector)
    {
        climb_vector = Vector;
        climb_position = Position;
    }

}
