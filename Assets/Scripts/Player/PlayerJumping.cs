﻿using UnityEngine;
using System.Collections;
using Helper;

public class PlayerJumping : MonoBehaviour {

    // Objects
    private Animator a;
    private Rigidbody r;
    private PlayerClimbing c;
    private PlayerMotor pm;
    private Transform t;

    // Jumping
    [SerializeField]
    private float JumpHeight = 1f;
    [SerializeField]
    private float JumpWidth = 3f;
    private bool is_jumping;
    private float jump_height;
    private float jump_width;

    /// <summary>
    /// Check if fall animation is playing
    /// </summary>
    public bool IsFalling { get { return a.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Fall); } }

    /// <summary>
    /// Check if jumping
    /// </summary>
    public bool IsJumping { get { return is_jumping; } }

    /// <summary>
    /// Awake
    /// </summary>
    void Awake()
    {
        a = GetComponent<Animator>();
        r = GetComponent<Rigidbody>();
        c = GetComponent<PlayerClimbing>();
        pm = GetComponent<PlayerMotor>();
        t = transform;        
    }
	
	/// <summary>
    /// Main update cycle
    /// </summary>
	void Update ()
    {
        a.SetBool(AnimatorConditions.Grounded, pm.IsGrounded);
        if (pm.IsGrounded && is_jumping)
        {
            is_jumping = false;
            AudioSource.PlayClipAtPoint(Sounds.FootStep, t.position, Settings.SoundVolume);
        }
    }

    /// <summary>
    /// Late update
    /// </summary>
    void LateUpdate()
    {
        if (c.State != PlayerClimbing.climb_state.None) is_jumping = false;
        if (is_jumping) JumpMovement();
        if (IsFalling || (!pm.IsGrounded && !is_jumping)) ApplyGravity();
    }

    /// <summary>
    /// Apply gravity
    /// </summary>
    private void ApplyGravity()
    {
        r.AddForce(Physics.gravity * 2.5f, ForceMode.Acceleration);
    }

    /// <summary>
    /// Jump movement
    /// </summary>
    private void JumpMovement()
    {
        float factor = 2f;
        float step = factor * Time.deltaTime;
        if (jump_height < JumpHeight / factor)
        {
            jump_height += step;
            t.position += (t.up * (JumpHeight * step));
        }
        else
        {
            t.position += (-t.up * (JumpHeight * step));
        }
        if (jump_width < JumpWidth / factor)
        {
            jump_width += step;
            t.position += (t.forward * (JumpWidth * step));
        }        
    }

    /// <summary>
    /// Start jump
    /// </summary>
    void StartJump()
    {
        t.position += t.up * 0.15f;
        t.parent = null;
        jump_height = 0f;
        jump_width = 0f;
        is_jumping = true;
        a.SetTrigger(AnimatorConditions.Jump, true);
        a.SetBool(AnimatorConditions.Grounded, false);
        AudioSource.PlayClipAtPoint(Sounds.FootJump, t.position, Settings.SoundVolume);
        //print("Start Jump");
    }

    /// <summary>
    /// Edge trigger
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer(Layers.EdgeJump) && !is_jumping)
        {
            float diff = Quaternion.Angle(col.gameObject.transform.rotation, t.rotation);
            if (diff < 90f)
            {
                if (pm.Speed > 0.5f)
                {
                    StartJump();
                }
                else
                {
                    //print("Hang");
                    c.LowerToClimb();
                }
            }                                
        }        
    }

    /// <summary>
    /// Play jump sound on animation
    /// </summary>
    /// <param name="evt"></param>
    public void Jump(AnimationEvent evt)
    {
        if (evt.animatorClipInfo.weight > 0.5f && evt.intParameter == a.GetDominantLayer())
        {
            AudioSource.PlayClipAtPoint(Sounds.FootJump, transform.position, Settings.SoundVolume);
        }
    }

}
