﻿using UnityEngine;
using System.Collections;
using Helper;

[RequireComponent(typeof(PlayerCharacter))]
[AddComponentMenu("Player/Attack")]
public class PlayerAttack : Pausable
{

    public enum AttackType { Horizontal, Vertical, Jump };

    //public GameObject Sword;

    public GameObject SwordHand;
    public GameObject TestBox;

	private PlayerMotor pm;
    private PlayerCamera cam;
	private Animator animator;

    private bool attack_was_playing;
	private bool is_executing_attack;

	/// <summary>
	/// Gets a value indicating whether player is executing attack.
	/// </summary>
	/// <value><c>true</c> if this instance is executing attack; otherwise, <c>false</c>.</value>
	public bool IsAttackPlaying { get { return animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.SwordStrikeHorizontal); } }

    /// <summary>
    /// Check if attack is executed
    /// </summary>
    public bool IsExecutingAttack { get { return is_executing_attack; } }
	
	/// <summary>
	/// Gets a value indicating whether this instance is repelling.
	/// </summary>
	/// <value><c>true</c> if this instance is repelling; otherwise, <c>false</c>.</value>
	public bool IsRepelling { get { return animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.SwordRepelHorizontal); } }

    /// <summary>
    /// Start
    /// </summary>
    public override void Start()
    {
        base.Start();
    }

    /// <summary>
    /// Initialize attack component.
    /// </summary>
    void Awake()
    {
		pm = GetComponent<PlayerMotor>();
        cam = GetComponent<PlayerCamera>();
        animator = GetComponent<Animator>();
	}

    /// <summary>
    /// Check for input to execute an attack.
    /// </summary>     
    void Update()
    {        
        if (!Paused)
        {
            if (!is_executing_attack)
            {
                if (cam.CameraState == CameraState.Target || cam.CameraState == CameraState.Normal)
                {
                    if (pm.IsWeaponEquiped && !pm.IsDrawWeaponPlaying)
                    {
                        if (Controls.Control.GetXDown()) ExecuteAttack();
                    }
                }
            }
            else
            {
                if (!attack_was_playing)
                {
                    if (IsAttackPlaying)
                    {
                        attack_was_playing = true;
                    }
                }
                else
                {
                    if (!IsAttackPlaying)
                    {
                        is_executing_attack = false;
                    }
                    else
                    {
                        bool atk_success; Vector3 atk_start; Vector3 atk_dir; RaycastHit atk_hit; Ray atk_ray;
                        atk_start = SwordHand.transform.position + (SwordHand.transform.right * 0.1f);
                        atk_dir = SwordHand.transform.right;
                        //TestBox.transform.position = atk_start + (SwordHand.transform.right * 0.75f);
                        atk_ray = new Ray(atk_start, atk_dir);
                        atk_success = Physics.Raycast(atk_ray, out atk_hit, 0.75f, LayerMask.GetMask("Geometry"));
                        if (atk_success)
                        {
                            TestBox.transform.position = atk_hit.point;
                            AudioSource.PlayClipAtPoint(Sounds.SwordRepel1, transform.position, 1f);
                            is_executing_attack = false;
                            animator.SetTrigger(AnimatorConditions.SwordRepel, true);
                        }
                    }
                }
            }
        }		
    }

	/// <summary>
	/// Attack the specified evt.
	/// </summary>
	/// <param name="evt">Evt.</param>
	public void Attack(AnimationEvent evt)
    {
        //is_executing_attack = true;
        if (evt.animatorClipInfo.weight > 0.5)
        {
			AudioSource.PlayClipAtPoint(Sounds.SwordSwing1, transform.position, Settings.SoundVolume);            
		}
	}

    public void AttackOver(AnimationEvent evt)
    {
        //is_executing_attack = false;
    }

	/// <summary>
	/// Repel the specified evt.
	/// </summary>
	/// <param name="evt">Evt.</param>
	public void Repel(AnimationEvent evt) {
		
	}

	/// <summary>
	/// Executes the attack.
	/// </summary>
	public void ExecuteAttack(AttackType Type = AttackType.Horizontal) {
        attack_was_playing = false;
        switch (Type)
        {
            case AttackType.Horizontal:
                animator.SetTrigger(AnimatorConditions.SwordStrikeHorizontal, true);
                break;
        }        
	}

}
