﻿using UnityEngine;
using System.Collections;
using Helper;

[RequireComponent(typeof(PlayerCharacter))]
[AddComponentMenu("Player/Attack")]
public class OLD_PlayerPlatforming : MonoBehaviour {

    private Animator animator;
    private PlayerMotor pm;
    private PlayerAttack attacking;

    // Climbing
    public float WaitWhenClimbing = 0.3f;
    private bool is_climbing;
    private bool can_climb;
    private Vector3 can_climb_vector = Vector3.zero;
    private float climb_timer;
    private Vector3 climb_position;
    private Vector3 climb_vector;
    private bool placed_for_climb;
    private bool is_dropping;

    // Leave platform
    private float step_from_edge = 0.05f;
    public float JumpWidth = 3f;
    public float JumpHeight = 1f;
    //private float fall_death_height = 10f;
    private bool is_jumping;
    private bool die_after_jump_match;
    private bool climb_after_jump_match;
    private jump_match jump_air_match;
    private jump_match jump_target_match;

    /// <summary>
    /// Check if player will leave the current platform.
    /// </summary>
    public bool WillLeavePlatform {
        get {
            bool will_leave = !Physics.Raycast(transform.position + (transform.forward * 0.05f), -transform.up, 0.5f, LayerMask.GetMask("Geometry"));
            //			bool on_slope = Physics.Raycast(transform.position + (transform.up * 0.2f), transform.forward, 1f, LayerMask.GetMask("Geometry"));
            //			bool on_slope2 = Physics.Raycast(transform.position + (transform.up * 0.2f) + (transform.forward * 0.1f), transform.up, 1f, LayerMask.GetMask("Geometry"));
            //return will_leave && pm.IsGrounded && !is_dropping;
            return will_leave && !is_dropping;
        }
    }

    /// <summary>
    /// Check if player is jumping.
    /// </summary>
    public bool IsJumping {
        get {
            bool jump = animator.GetBool(AnimatorConditions.Jump) || animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Jump);
            bool jump_edge = animator.GetBool(AnimatorConditions.JumpEdge) || animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.JumpEdge);
            bool jump_death = animator.GetBool(AnimatorConditions.JumpDeath) || animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.JumpDeath);
            return jump || jump_death || jump_edge;
        }
    }

    /// <summary>
    /// Check if player is rolling.
    /// </summary>
    public bool IsClimbing {
        get {
            bool hang = animator.GetBool(AnimatorConditions.Hang) || animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Hang);
            bool climb = animator.GetBool(AnimatorConditions.Climb) || animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Climb);
            return hang || climb;
        }
    }

    public bool IsFalling
    {
        get
        {
            return animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Fall);
        }
    }

    // Use this for initialization
    void Awake() {
        animator = GetComponent<Animator>();
        pm = GetComponent<PlayerMotor>();
        attacking = GetComponent<PlayerAttack>();
    }

    //void PlatformParent()
    //{
    //    RaycastHit hit;
    //    bool success = Physics.Raycast(transform.position, - Vector3.up, out hit, 0.1f, LayerMask.GetMask("Geometry"));
    //    if (success)
    //    {
    //        transform.parent = hit.transform;
    //    }
    //}

    // Update is called once per frame
    void Update() {

        if (pm.IsGrounded)
        {
            is_dropping = false;
            //PlatformParent();
        }

        if (IsJumping) {
            jumping();
            return;
        }

        if (is_climbing) {
            climbing();
            return;
        }

        if (can_climb) {
            wait_for_climb();
            return;
        }

        // If player will leave platform start mechanic
        if (WillLeavePlatform) {
            // Get input values from the left stick
            //			float speed = Mathf.Abs(Controls.Control.GetLH()) + Mathf.Abs(Controls.Control.GetLV());
            leaving_platform(pm.Speed);
        }

        //		if (!pm.IsGrounded) {
        //			CharacterController cc = GetComponent<CharacterController>();
        //			cc.Move(new Vector3(0f, -9.81f, 0f));
        //		}

        //if (!pm.IsGrounded && !is_jumping) {
        if (IsFalling) {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.AddForce(-transform.up * 9.81f, ForceMode.Acceleration);
        }

        if (!pm.IsGrounded && transform.position.y <= -50f) {
            transform.position = new Vector3(-1.6f, 1.3f, 5.33f);
        }

    }

    public void Jump(AnimationEvent evt) {
        if (evt.animatorClipInfo.weight > 0.5) {
            AudioSource.PlayClipAtPoint(Sounds.FootJump, transform.position, 1f);
        }
    }

    /// <summary>
    /// Start climbing trigger.
    /// </summary>
    private void StartClimbing() {
        //		SetTrigger(AnimatorConditions.Hang);
        animator.SetTrigger(AnimatorConditions.Hang, true);
        placed_for_climb = false;
        is_climbing = true;
    }

    private void DirectClimbing(Vector3 Position, Vector3 Vector)
    {
        transform.forward = Vector;
        transform.position = Position;
        //animator.SetTrigger(AnimatorConditions.Hang, true);
        animator.SetTrigger(AnimatorConditions.Climb, true);
    }

    /// <summary>
    /// Start climbing trigger.
    /// </summary>
    private void StartClimbing(Vector3 Position, Vector3 Vector) {
        PrepareClimbing(Position, Vector);
        StartClimbing();
    }

    /// <summary>
    /// Prepare climbing trigger with data.
    /// </summary>
    private void PrepareClimbing(Vector3 Position, Vector3 Vector) {
        climb_vector = Vector;
        climb_position = Position;
        climb_timer = WaitWhenClimbing;        
    }

    /// <summary>
    /// Climbing mechanic.
    /// Input is needed to switch from hang to to climb state.
    /// </summary>
    private void climbing() {
        if (!animator.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Climb))
        {
            if (!placed_for_climb)
            {
            //if (!is_climbing) {
            // Rotation
            transform.forward = climb_vector;
            //			transform.rotation = climb_target_match.Rotation;
            // Position
            //			player_rigidbody.position = climb_position;
            transform.position = climb_position;
                //			player_rigidbody.position = climb_target_match.Position;

                //			horizontal = Controls.Control.GetLH();
                //			vertical = Controls.Control.GetLV();			
                //			move_direction = new Vector3(horizontal, 0f, vertical);
                //			move_direction = cam.TransformDirection(move_direction);
                //			move_direction.y = 0.0f;
                placed_for_climb = true;
            }

            if (climb_timer <= 0f) {
                float diff = Vector3.Angle(pm.InputDirection, climb_vector);
                if (diff < 30f) {
                    // Trigger Climbing
                    //					SetTrigger(AnimatorConditions.ClimbUp);
                    //					animator.ResetTrigger(AnimatorConditions.Hang);
                    animator.SetTrigger(AnimatorConditions.Climb, true);
                } else if (diff > 135f) {
                    if (!is_dropping)
                    {
                        //print("Down Diff=" + diff.ToString());
                        is_dropping = true;
                        is_climbing = false;
                        climb_timer = WaitWhenClimbing;
                        //StartCoroutine(Drop());
                        transform.position = transform.position + (-transform.forward * 0.5f) + (-transform.up * 0.735f);
                        animator.SetTrigger(AnimatorConditions.Drop, true);
                    }
                }
            } else {
                climb_timer -= Time.deltaTime;
            }
        } else {
            is_climbing = false;
        }
    }

    //IEnumerator Drop()
    //{
    //    yield return new WaitForEndOfFrame();
    //    transform.position = transform.position + (-transform.forward * 0.5f); // + (-transform.up * 0.735f);
    //    animator.SetTrigger(AnimatorConditions.Drop, true);
    //    //is_dropping = true;
    //}

    private float wall_detection_length = 1f;
    private float wall_y_offset = 0.3f;
    private float max_climb_height = 3f;
    private float minimum_step_size = 0.1f;
    private float maxmimum_step_size = 2f;
    private float mid_step_size = 1f;

    /// <summary>
    /// Check for collisions to jump up platforms.
    /// </summary>
    void OnCollisionEnter(Collision col) {
        //		can_climb = true;
        //		bool success; Vector3 start; Vector3 dir; Ray ray; RaycastHit hit = new RaycastHit();
        if (pm.IsGrounded && !IsJumping && !pm.IsRollPlaying && !attacking.IsExecutingAttack && !attacking.IsRepelling) {
            if (col.gameObject.layer == LayerMask.NameToLayer("Geometry") && col.contacts[0].point.y > transform.position.y + 0.2f) {
                can_climb = true;
                can_climb_vector = col.contacts[0].normal;
                climb_timer = WaitWhenClimbing;
                //				print("Can now climb");
                //				start = transform.position + (transform.up * wall_y_offset);
                //				dir = transform.forward;
                //				ray = new Ray(start, dir);
                //				success = Physics.Raycast(ray, out hit, wall_detection_length, LayerMask.GetMask("Geometry"));
                //				if (success) {
                //					//Make a reaycast to get a position to climb up to
                //					Vector3 start_down = hit.point + (hit.transform.up * max_climb_height) + (-hit.normal * step_from_edge);
                //					Vector3 dir_down = -Vector3.up;
                //					Ray ray_down = new Ray(start_down, dir_down);
                //					RaycastHit hit_down = new RaycastHit();
                //					if (Physics.Raycast(ray_down, out hit_down, max_climb_height, LayerMask.GetMask("Geometry"))) {
                //						//						print("Success");
                //						float diff = hit_down.point.y - transform.position.y;
                //						if (diff > mid_step_size && diff < maxmimum_step_size) {
                //							//							jump_target_match = new jump_match(hit_down.point - (Vector3.up * 0.735f), transform.rotation, 0f, 1f);
                //							jump_air_match = new jump_match(hit_down.point - (Vector3.up * 0.735f), Quaternion.LookRotation(-hit.normal), 0f, 0.8f);
                //							jump_target_match = new jump_match(hit_down.point, transform.rotation, 0.8f, 1f);
                //							PrepareClimbing(hit_down.point, -hit.normal);
                //							climb_after_jump_match = true;
                //							is_jumping = true;
                //							//							SetTrigger(AnimatorConditions.JumpEdge);
                //							animator.SetTrigger(AnimatorConditions.JumpEdge, true);
                //						} else if (diff > minimum_step_size && diff < mid_step_size) {
                //							StartClimbing(hit_down.point, -hit.normal);
                //						}
                //					}
                //				}
            }
        }
    }

    void OnCollisionStay(Collision col)
    {
        //if (col.gameObject.tag == "Geometry")
        //{

        if (transform.parent != col.transform)
        {
            if (col.gameObject.layer == LayerMask.NameToLayer("Geometry"))
            {
                RaycastHit hit;
                bool success = Physics.Raycast(transform.position, -Vector3.up, out hit, 0.1f, LayerMask.GetMask("Geometry"));
                if (success)
                {
                    if (hit.transform == col.transform)
                    {
                        //transform.parent = col.transform;
                        transform.SetParent(col.transform, true);
                        print("Set Parent");
                    }
                }
            }
        }           
                  
        //}
        //else
        //{
        //    transform.parent = null;
        //}

        if (!pm.IsGrounded && IsFalling)
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            Vector3 fix_dir = col.contacts[0].normal;
            fix_dir.y = 0f;
            rb.AddForce(fix_dir * 9.81f, ForceMode.VelocityChange);
        }

    }

	void OnCollisionExit() {
        //transform.parent = null;
        if (can_climb) {
//			print("Climb aborted");
			can_climb = false;
		}
	}

	void wait_for_climb() {
		bool success; Vector3 start; Vector3 dir; Ray ray; RaycastHit hit = new RaycastHit();
		if (can_climb) {
//			print("Waiting for climb");
//			animator.SetFloat(AnimatorConditions.Speed, 0f);
			if (climb_timer <= 0f) {
				if (pm.IsGrounded && !IsJumping && !pm.IsRollPlaying && !attacking.IsExecutingAttack && !attacking.IsRepelling) {
//					if (col.gameObject.layer == LayerMask.NameToLayer("Geometry")) {
						start = transform.position + (transform.up * wall_y_offset);
						dir = transform.forward;
						ray = new Ray(start, dir);
						success = Physics.Raycast(ray, out hit, wall_detection_length, LayerMask.GetMask("Geometry"));
						if (success) {
							//Make a reaycast to get a position to climb up to
							Vector3 start_down = hit.point + (hit.transform.up * max_climb_height) + (-hit.normal * step_from_edge);
							Vector3 dir_down = -Vector3.up;
							Ray ray_down = new Ray(start_down, dir_down);
							RaycastHit hit_down = new RaycastHit();
							if (Physics.Raycast(ray_down, out hit_down, max_climb_height, LayerMask.GetMask("Geometry"))) {
//								print("Success");
								float diff = hit_down.point.y - transform.position.y;
								if (diff > mid_step_size && diff < maxmimum_step_size) {
									//							jump_target_match = new jump_match(hit_down.point - (Vector3.up * 0.735f), transform.rotation, 0f, 1f);
									jump_air_match = new jump_match(hit_down.point - (Vector3.up * 0.735f), Quaternion.LookRotation(-hit.normal), 0f, 0.8f);
									jump_target_match = new jump_match(hit_down.point, transform.rotation, 0.8f, 1f);
									PrepareClimbing(hit_down.point, -hit.normal);
									climb_after_jump_match = true;
									is_jumping = true;
									//							SetTrigger(AnimatorConditions.JumpEdge);
									animator.SetTrigger(AnimatorConditions.JumpEdge, true);
								} else if (diff > minimum_step_size && diff < mid_step_size) {
                                    DirectClimbing(hit_down.point, -hit.normal);
								}
							}
						}
//					}
				} else {
//					print("Climb aborted");
					can_climb = false;
				}
			} else {
				if (pm.InputDirection != Vector3.zero && Vector3.Angle(-can_climb_vector, pm.InputDirection) < 45f) {
					climb_timer -= Time.deltaTime;
				} else {
					climb_timer = WaitWhenClimbing;
				}
			}
		}
	}

	private class jump_match {
		public Vector3 Position;
		public Quaternion Rotation;
		public MatchTargetWeightMask weight;
		public float From;
		public float To;
		public jump_match(Vector3 Position, Quaternion Rotation, float From, float To) {
			this.Position = Position;
			this.Rotation = Rotation;
			this.weight = new MatchTargetWeightMask(Vector3.one, 1f);
			this.From = From;
			this.To = To;
		}
	}
	
	/// <summary>
	/// Is called when character is leaving the platform he is currently standing on.
	/// It decides between jumping and lowering to hang based on the speed.
	/// </summary>
	private void leaving_platform(float Speed) {
		if (Speed >= 0.5f) {
			if (!is_jumping) start_jump();
		} else {
			lower_to_hang();
		}
	}
	
	public GameObject test_box;
	
	/// <summary>
	/// Do match targeting for jumping.
	/// </summary>
	private void jumping() {
		if (!animator.IsInTransition(0)) {
			if (jump_air_match != null) {
				animator.MatchTarget(jump_air_match.Position, jump_air_match.Rotation, AvatarTarget.Root, jump_air_match.weight, jump_air_match.From, jump_air_match.To);
			}
			if (jump_target_match != null) {
				animator.MatchTarget(jump_target_match.Position, jump_target_match.Rotation, AvatarTarget.Root, jump_target_match.weight, jump_target_match.From, jump_target_match.To);
			}
		} else {
			is_jumping = false;
			if (die_after_jump_match) {
				die_after_jump_match = false;
				// Respawn player
				transform.position = new Vector3(-1.6f, 1.3f, 5.33f);
			} else if (climb_after_jump_match) {
				climb_after_jump_match = false;
				StartClimbing();
			}
		}
	}
	
	/// <summary>
	/// Trigger jump when leaving the current platform.
	/// Tries to predict a contact point in mid air, then a target on the ground.
	/// If prediction fails an unpredicted jump is triggered.
	/// </summary>
	private void start_jump() {
		bool success = false; Vector3 start; Vector3 dir; Ray ray; RaycastHit hit = new RaycastHit(); RaycastHit hit_down = new RaycastHit();
		RaycastHit hit_up = new RaycastHit(); bool success_up; bool success_forward;
		
		// ##### Test if collision with surface can be predicted #####
		// Check if way is free
		start = transform.position + (transform.up * JumpHeight);
		dir = transform.forward;
		ray = new Ray(start, dir);
		//		print("Jump to platform");
		success_forward = Physics.Raycast(ray, out hit, JumpWidth, LayerMask.GetMask("Geometry"));
		if (!success_forward) {
			start = transform.position + (transform.up * JumpHeight) + (transform.forward * JumpWidth);
			dir = -Vector3.up;
			ray = new Ray(start, dir);
			success = Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Geometry"));
			if (success) {
				//				test_box.transform.position = hit.point;
				// Check if above is free
				dir = Vector3.up;
				ray = new Ray(start, dir);
				success_up = Physics.Raycast(ray, out hit_up, Mathf.Infinity, LayerMask.GetMask("Geometry"));
				if (success_up) {
					if (hit_up.point.y < hit.point.y + 4f) {
						//						print("Not enough place under platform");
						success = false;
					}
				}
				if (success) {
					//					print("Success");
					jump_air_match = new jump_match(transform.position + (transform.up * JumpHeight) + (transform.forward * (JumpWidth * 0.5f)),
					                                transform.rotation, 0f, 0.5f);
					jump_target_match = new jump_match(hit.point, transform.rotation, 0.5f, 1f);
					is_jumping = true;
					//					SetTrigger(AnimatorConditions.Jump);
					animator.SetTrigger(AnimatorConditions.Jump, true);
				}
			}
		}
		
		
		// ##### Test if collision with platform edge can be predicted #####
		if (!success) {
			start = transform.position + (transform.up * (JumpHeight * 2f)) + (transform.forward * (JumpWidth * 1.5f));
			//			test_box.transform.position = start;
			dir = -Vector3.up;
			ray = new Ray(start, dir);
			//			print("Jump on edge");
			success = Physics.Raycast(ray, out hit_down, Mathf.Infinity, LayerMask.GetMask("Geometry"));
			if (success) {
				// Do a new raycast to get the edge of the predeicted platform
				start = hit_down.point + (-transform.forward * JumpWidth) + (-transform.up * 0.1f);
				//				test_box.transform.position = start;
				dir = transform.forward;
				ray = new Ray(start, dir);
				success = Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Geometry"));
				if (success) {
					// Do a new raycast to get the position on the edge of the predeicted platform
					start = hit.point + (Vector3.up * 1f) + (-hit.normal * step_from_edge);
					//					test_box.transform.position = start;
					dir = -Vector3.up;
					ray = new Ray(start, dir);
					success = Physics.Raycast(ray, out hit_down, Mathf.Infinity, LayerMask.GetMask("Geometry"));
					if (success) {
						//						print("Success");
						jump_target_match = new jump_match(hit_down.point - (Vector3.up * 0.735f), transform.rotation, 0.5f, 1f); // Quaternion.LookRotation(-hit.normal), 0f, 1f);
						//						test_box.transform.position = ((transform.position + hit_down.point) / 2f) + Vector3.up * (JumpHeight * 0.5f);
						jump_air_match = new jump_match(((transform.position + hit_down.point) / 2f) + Vector3.up * (JumpHeight * 0.5f), transform.rotation, 0f, 0.5f);
						PrepareClimbing(hit_down.point, -hit.normal);
						climb_after_jump_match = true;
						is_jumping = true;
						//						SetTrigger(AnimatorConditions.JumpEdge);
						animator.SetTrigger(AnimatorConditions.JumpEdge, true);
					}
				}
			}
		}
		
		// ##### If everything else fails do an unpredicted jump #####
		if (!success) {
			//			print("Jump to death");
			jump_air_match = new jump_match(transform.position + (transform.up * JumpHeight) + (transform.forward * (JumpWidth * 0.5f)),
			                                transform.rotation, 0f, 0.5f);
			jump_target_match = new jump_match(transform.position + (transform.forward * JumpWidth) + (-transform.up * 0f), //fall_death_height),
			                                   transform.rotation, 0.5f, 1f);
//			die_after_jump_match = true;
			is_jumping = true;
			//			SetTrigger(AnimatorConditions.JumpDeath);
			animator.SetTrigger(AnimatorConditions.JumpDeath, true);
		}
		
	}
	
	/// <summary>
	/// Go to hang animation when walking over an edge slow.
	/// </summary>
	private void lower_to_hang() {
		bool success; Vector3 start; Vector3 dir; Ray ray; RaycastHit hit = new RaycastHit();
		Vector3 start_down; Vector3 dir_down; Ray ray_down; RaycastHit hit_down = new RaycastHit();
		
		start = transform.position + (transform.forward * wall_y_offset) + (-transform.up * wall_y_offset);
		dir = -transform.forward;
		ray = new Ray(start, dir);
		success = Physics.Raycast(ray, out hit, wall_detection_length, LayerMask.GetMask("Geometry"));
		if (success) {
			// Make a reaycast to get a position to climb up to
			start_down = hit.point + (hit.transform.up * max_climb_height) + (-hit.normal * step_from_edge);
			dir_down = hit.transform.up * -1f;
			ray_down = new Ray(start_down, dir_down);
			if (Physics.Raycast(ray_down, out hit_down, max_climb_height, LayerMask.GetMask("Geometry"))) {
				StartClimbing(hit_down.point, -hit.normal);
			}
		}
	}

}
