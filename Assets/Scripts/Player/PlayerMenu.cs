﻿using UnityEngine;
using System.Collections;

public class PlayerMenu : MonoBehaviour {

    // Object
    private menu_in_game_hud hud;
    private PlayerMotor pm;
    private PlayerCamera cam;
    private PlayerClimbing cl;

    /// <summary>
    /// Start
    /// </summary>
    private void Start()
    {
        hud = GameObjectTools.GetComponentInChildren<menu_in_game_hud>(FindObjectOfType<menu_in_game>().gameObject);
        pm = GetComponent<PlayerMotor>();
        cam = GetComponent<PlayerCamera>();
        cl = GetComponent<PlayerClimbing>();
    }

    /// <summary>
    /// Main update cycle
    /// </summary>
    private void Update()
    {
        hud.SetAction(menu_in_game_hud.ActionButton.A, menu_in_game_hud.Action.None);
        hud.SetAction(menu_in_game_hud.ActionButton.X, menu_in_game_hud.Action.None);
        if (pm.Speed > 0.5f)
        {
            if (!pm.IsRollPlaying && cl.State == PlayerClimbing.climb_state.None)
            {
                hud.SetAction(menu_in_game_hud.ActionButton.A, menu_in_game_hud.Action.Roll);
            }
            if (cam.CameraState == Helper.CameraState.Target)
            {
                if (pm.IsWeaponEquiped)
                {
                    hud.SetAction(menu_in_game_hud.ActionButton.A, menu_in_game_hud.Action.Jump);
                }
            }
        }
        else if (pm.Direction > 0.5f || pm.Direction < -0.5f)
        {
            if (cam.CameraState == Helper.CameraState.Target)
            {
                hud.SetAction(menu_in_game_hud.ActionButton.A, menu_in_game_hud.Action.Dodge);
            }                
        }
        else if (pm.Speed < -0.5f)
        {
            if (cam.CameraState == Helper.CameraState.Target)
            {
                hud.SetAction(menu_in_game_hud.ActionButton.A, menu_in_game_hud.Action.Dodge);
            }
        }
        else
        {
            if (pm.IsWeaponEquiped)
            {
                hud.SetAction(menu_in_game_hud.ActionButton.A, menu_in_game_hud.Action.Sheath);
            }
            if (cam.FocusTarget.HasConversation())
            {
                hud.SetAction(menu_in_game_hud.ActionButton.A, menu_in_game_hud.Action.Speak);
            }
        }        
        if (pm.IsWeaponEquiped)
        {
            hud.SetAction(menu_in_game_hud.ActionButton.X, menu_in_game_hud.Action.Attack);
        }
        else
        {
            hud.SetAction(menu_in_game_hud.ActionButton.X, menu_in_game_hud.Action.Draw);
        }
    }

}
