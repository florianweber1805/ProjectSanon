﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Helper;

[RequireComponent(typeof(PlayerCharacter))]
[AddComponentMenu("Player/Motor")]
public class PlayerMotor : Pausable {

    // Objects
    private Transform t;
    private Animator a;
    private PlayerClimbing cl;	
	private PlayerCamera cam;
	private PlayerAttack atk;
    private PlayerJumping j;
    private menu_in_game m;
    private menu_in_game_conversation con;

	// Weapon / Shield
    [SerializeField]
	private GameObject sword_holstered;
    [SerializeField]
    private GameObject shield_holstered;
    [SerializeField]
    private GameObject sword;
    [SerializeField]
    private GameObject shield;
    private bool is_weapon_equiped;

    // Movement
    private float horizontal = 0f;
	private float vertical = 0f;
	private float speed = 0f;
    private float direction = 0f;
    private bool null_speed;
	private Vector3 move_direction = Vector3.zero;
	private Vector3 input_direction = Vector3.zero;

    // Dodge
    private float dodge_direction;
    private float dodge_speed;

    /// <summary>
    /// Check if player is grounded.
    /// </summary>
    public bool IsGrounded { get { return (Physics.Raycast(transform.position, -Vector3.up, 0.1f, LayerMask.GetMask("Geometry"))); } }

    /// <summary>
    /// Expose current speed
    /// </summary>
	public float Speed { get { return speed; } }

    public float Direction
    {
        get
        {
            return direction;
        }
    }

    /// <summary>
    /// Expose current move direction
    /// </summary>
	public Vector3 MoveDirection { get { return move_direction; } }

    /// <summary>
    /// Expose current input direction
    /// </summary>
	public Vector3 InputDirection { get { return input_direction; } }

    /// <summary>
    /// Check if rolling animation is playing
    /// </summary>
	public bool IsRollPlaying { get { return a.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Roll); } }

    /// <summary>
    /// Check if draw weapon animation is playing
    /// </summary>
	public bool IsDrawWeaponPlaying { get { return a.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.DrawWeapon) || a.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.SheathWeapon); } }

    public bool IsDodgePlaying
    {
        get
        {
            return a.GetCurrentAnimatorStateInfo(0).IsName(AnimationStates.Dodge);
        }
    }

    /// <summary>
    /// Expose weapon state
    /// </summary>
	public bool IsWeaponEquiped { get { return is_weapon_equiped; } }

	/// <summary>
	/// Initialize player movement.
	/// </summary>
	void Awake() {
        //Time.timeScale = 0.33f;
        t = transform;
        a = GetComponent<Animator>();
        cl = GetComponent<PlayerClimbing>();
        cam = GetComponent<PlayerCamera>();
        atk = GetComponent<PlayerAttack>();
        j = GetComponent<PlayerJumping>();
        move_direction = transform.forward;
	}

    public override void Start()
    {
        base.Start();
        m = FindObjectOfType<menu_in_game>();
        //hud = GameObjectTools.GetComponentInChildren<menu_in_game_hud>(m.gameObject);
        con = GameObjectTools.GetComponentInChildren<menu_in_game_conversation>(m.gameObject);

        // ######################################################################################################## TEMP
        Data.OpenDB();
        // ######################################################################################################## TEMP
    }

    // ######################################################################################################## TEMP
    void OnApplicationQuit() { Data.CloseDB(); }
    // ######################################################################################################## TEMP

    /// <summary>
    /// Main update cycle.
    /// </summary>
    void Update () {
		input_direction = Vector3.zero;

        if (!Paused)
        {
            // ######################################################################################################## TEMP
            if (Controls.Control.GetStartDown()) { m.Show(menu_in_game.MenuPart.Items); }
            if (Controls.Control.GetBackDown()) { Application.LoadLevel(Scenes.MainMenu); }
            // ######################################################################################################## TEMP

            // Movement
            switch (cam.CameraState)
            {
                case CameraState.Normal:
                    NormalMovement();
                    break;
                case CameraState.Target:
                    TargetMovement();
                    break;
                case CameraState.Speaking:
                case CameraState.FirstPerson:
                    Stationary();
                    break;
            }

        }
        else
        {
            a.SetFloat(AnimatorConditions.Speed, 0f);
        }

        // Null speed when colliding with obstacle
        if (null_speed && cl.State != PlayerClimbing.climb_state.Jump)
        {
            a.SetFloat(AnimatorConditions.Speed, 0f);
        }

	}

    // ####################################################################################################################################### Movement

    /// <summary>
    /// Normal movement
    /// </summary>
    void NormalMovement()
    {
        // Set direction value to 0 for normal camera
        a.SetFloat(AnimatorConditions.Direction, 0f);

        // Get input values from the left stick
        horizontal = Controls.Control.GetLH();
        vertical = Controls.Control.GetLV();

        // Calculate input speed
        speed = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
        direction = 0f;

        // Calculate move direction
        input_direction = new Vector3(horizontal, 0f, vertical);
        if (input_direction != Vector3.zero)
        {
            input_direction = cam.TransformDirection(input_direction);
            input_direction.y = 0.0f;
            move_direction = input_direction;
        }

        if (IsGrounded)
        {
            // Set animation speed
            a.SetFloat(AnimatorConditions.Speed, speed);

            // Rotate character to movement direction
            if (!IsRollPlaying && !atk.IsRepelling && cl.State == PlayerClimbing.climb_state.None)
            {
                transform.rotation = Quaternion.LookRotation(move_direction.normalized);
            }

            // Roll
            if (speed >= 0.5f && !IsRollPlaying)
            {
                if (Controls.Control.GetADown()) a.SetTrigger(AnimatorConditions.Roll, true);
            }

            // Sheath weapon
            AllowSheathWeapon();
        }

        // Attack
        AllowDrawWeapon();
    }

    /// <summary>
    /// Target movement
    /// </summary>
    void TargetMovement()
    {
        if (IsGrounded)
        {
            // Set move direction to current direction
            move_direction = t.forward;

            // Get input values from the left stick
            horizontal = Controls.Control.GetLH();
            vertical = Controls.Control.GetLV();

            // Speed and Direction
            //speed = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
            speed = vertical;
            direction = horizontal;

            // Set animation speed and direction
            a.SetFloat(AnimatorConditions.Speed, vertical);
            a.SetFloat(AnimatorConditions.Direction, horizontal);

            // Dodge
            if ((direction > 0.5f || direction < -0.5f || speed < -0.5f) && !IsDodgePlaying)
            {
                if (Controls.Control.GetADown())
                {
                    a.SetTrigger(AnimatorConditions.Dodge, true);
                    dodge_direction = direction;
                    dodge_speed = speed;
                }
            }
            else if (IsDodgePlaying)
            {
                a.SetFloat(AnimatorConditions.Speed, dodge_speed);
                a.SetFloat(AnimatorConditions.Direction, dodge_direction);
            }

            // Jump
            if (speed > 0.5f && !IsDodgePlaying)
            {
                // Jump
            }

            // Speak
            if (cam.FocusTarget.HasConversation() && !IsDodgePlaying && speed == 0f && direction == 0f)
            {
                if (Controls.Control.GetADown())
                {
                    // Sheath weapon if drawn
                    if (is_weapon_equiped) a.SetTrigger(AnimatorConditions.DrawWeapon, true);

                    // Set animation speed and direction to 0
                    a.SetFloat(AnimatorConditions.Speed, 0f);
                    a.SetFloat(AnimatorConditions.Direction, 0f);

                    // Pause game
                    //Pausing.Pause(Pausing.PauseType.Game);
                    // Set camera to speak
                    cam.ToggleSpeaking();
                    // Rotate npc
                    cam.FocusTarget.Conversation().BaseModel.transform.rotation =
                        Quaternion.LookRotation(cam.FocusTarget.Conversation().BaseModel.transform.position.Direction(t.position));
                    // Open conversation
                    m.Show(menu_in_game.MenuPart.Conversation);
                    con.Open(cam.FocusTarget.Conversation());
                }
            }
            

            // Sheath weapon
            AllowSheathWeapon();
        }
        else
        {
            // Set animation speed and direction to 0
            a.SetFloat(AnimatorConditions.Speed, 0f);
            //a.SetFloat(AnimatorConditions.Direction, 0f);
        }

        // Attack
        AllowDrawWeapon();
    }

    /// <summary>
    /// Stationary movement
    /// </summary>
    void Stationary()
    {
        // Set move direction to current direction
        move_direction = t.forward;

        // Set animation speed and direction to 0
        a.SetFloat(AnimatorConditions.Speed, 0f);
        a.SetFloat(AnimatorConditions.Direction, 0f);
    }

    /// <summary>
    /// Draw weapon
    /// </summary>
    void AllowDrawWeapon()
    {
        if (!is_weapon_equiped && !IsDrawWeaponPlaying)
        {
            if (Controls.Control.GetXDown()) a.SetTrigger(AnimatorConditions.DrawWeapon, true);
        }
    }

    void AllowSheathWeapon()
    {
        if (is_weapon_equiped && direction == 0f && speed == 0f && !IsDrawWeaponPlaying)
        {
            if (Controls.Control.GetADown()) a.SetTrigger(AnimatorConditions.DrawWeapon, true);
        }
    }

    // ####################################################################################################################################### Collisions

    /// <summary>
    /// Collisions
    /// </summary>
    /// <param name="col"></param>
    //void OnCollisionEnter(Collision col)
    //{
    //    if (t.parent != col.transform)
    //    {
    //        if (col.contacts[0].point.y <= t.position.y)
    //        {
    //            t.SetParent(col.transform, true);
    //        }
    //    }
    //}

    /// <summary>
    /// Collisions
    /// </summary>
    /// <param name="col"></param>
    void OnCollisionStay(Collision col)
    {
        null_speed = false;
        if (col.contacts[0].point.y > transform.position.y + 0.2f)
        {
            if (Vector3.Angle(input_direction, -col.contacts[0].normal) < 90f)
            {
                null_speed = true;
            }
            else
            {
                null_speed = false;
            }
        }
        if (t.parent != col.transform && !j.IsJumping)
        {
            //if (col.contacts[0].point.y <= t.position.y)
            //{
            t.SetParent(col.transform, true);
            //}
        }
    }

    /// <summary>
    /// Collision ends
    /// </summary>
    /// <param name="col"></param>
    //void OnCollisionExit(Collision col)
    //{
    //    if (col.gameObject.layer == LayerMask.NameToLayer("Geometry"))
    //    {
    //        if (transform.parent == col.transform)
    //        {
    //            //transform.parent = null;
    //        }
    //    }
    //}

    // ####################################################################################################################################### Animation Events

    /// <summary>
    /// Draw weapon
    /// Triggered when hand reaches sword position in draw animation
    /// </summary>
    /// <param name="evt"></param>
    public void DrawWeapon(AnimationEvent evt)
    {
        if (evt.animatorClipInfo.weight > 0.5f) //&& evt.intParameter == a.GetDominantLayer())
        {
            if (!is_weapon_equiped)
            {
                // ######################################################################################################## TEMP
                sword.SetActive(true);
                sword_holstered.SetActive(false);
                shield.SetActive(true);
                shield_holstered.SetActive(false);
                // ######################################################################################################## TEMP
                AudioSource.PlayClipAtPoint(Sounds.SwordDraw, transform.position, Settings.SoundVolume);
                a.SetBool(AnimatorConditions.WeaponIsOut, true);                
                a.SetLayerWeight(0, 0f);
                a.SetLayerWeight(1, 1f);
            }
            else
            {
                // ######################################################################################################## TEMP
                sword.SetActive(false);
                sword_holstered.SetActive(true);
                shield.SetActive(false);
                shield_holstered.SetActive(true);
                // ######################################################################################################## TEMP
                AudioSource.PlayClipAtPoint(Sounds.SwordSheath, transform.position, Settings.SoundVolume);
                a.SetBool(AnimatorConditions.WeaponIsOut, false);
                a.SetLayerWeight(0, 1f);
                a.SetLayerWeight(1, 0f);
            }
            is_weapon_equiped = !is_weapon_equiped;
        }
    }

    /// <summary>
    /// Equipment sounds
    /// Triggered once in the walk cycle when weapon is on the back
    /// </summary>
    /// <param name="evt"></param>
	public void FootSword(AnimationEvent evt)
    {
        if (!is_weapon_equiped)
        {
            if (evt.animatorClipInfo.weight > 0.5 && evt.intParameter == a.GetDominantLayer())
            {
                int rnd = Random.Range(0, 3);
                if (evt.intParameter == 1) rnd = Random.Range(0, 1);
                switch (rnd)
                {
                    case 0:
                        AudioSource.PlayClipAtPoint(Sounds.SwordWalk1, transform.position, Settings.SoundVolume * 0.25f);
                        break;
                    case 1:
                        AudioSource.PlayClipAtPoint(Sounds.SwordWalk2, transform.position, Settings.SoundVolume * 0.25f);
                        break;
                }
            }
        }
    }

    /// <summary>
    /// Footstep sounds
    /// Triggered on every foot step
    /// </summary>
    /// <param name="evt"></param>
	public void Foot(AnimationEvent evt)
    {
        if (evt.animatorClipInfo.weight > 0.5f && evt.intParameter == a.GetDominantLayer())
        {
            AudioSource.PlayClipAtPoint(Sounds.FootStep, transform.position, Settings.SoundVolume);
        }
    }

    /// <summary>
    /// Rolling sounds
    /// Triggered once in the rolling animation
    /// </summary>
    /// <param name="evt"></param>
	public void Roll(AnimationEvent evt)
    {
        if (evt.animatorClipInfo.weight > 0.5f && evt.intParameter == a.GetDominantLayer())
        {
            AudioSource.PlayClipAtPoint(Sounds.Roll, transform.position, Settings.SoundVolume);
        }
    }

}
