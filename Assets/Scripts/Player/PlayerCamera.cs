﻿using UnityEngine;
using System.Collections;
using Helper;

[AddComponentMenu("Player/PlayerCharacter")]
public class PlayerCamera : Pausable
{

    // Objects
    private Animator a;
    private Camera cam;
    private menu_in_game_hud hud;
    private CameraObject camera_default_target_object;
    private CameraObject camera_target_object;
    private CameraObject camera_mount_point;
    private GameObject focus_target;
    private PlayerMotor motor;

    private CameraState camera_state = CameraState.Normal;

    // Field of View
    [SerializeField]
    private float NormalFOV = 60f;
    [SerializeField]
    private float ZoomFOV = 50f;
    [SerializeField]
    private float fov_speed = 10f;
    private float fov_lerp = 1f;
    private float fov;
    private bool is_zoomed_in;

    // Cam position change
    [SerializeField]
    private float cam_speed = 2f;
    private float cam_lerp;
    [SerializeField]
    private float cam_target_speed = 500f;

    // Aim shield
    [SerializeField]
    private GameObject[] aim_bones;
    [SerializeField]
    private GameObject[] counter_aim_bones;
	private float hip_rotationX = 0f;
	private float hip_rotationY = 0f;
	private bool do_late_hip_rotation;
    [SerializeField]
    private float HipMaxAngleX = 60f;
    [SerializeField]
    private float HipMaxAngleY = 40f;

	// Position
	private Vector3 normal_camera_position = new Vector3(0f, 1f, 3.5f);

	// Rotation
    [SerializeField]
	private float Sensitivity = 150f;
    [SerializeField]
    private float MinimumAngle = -40f;
    [SerializeField]
    private float MaximumAngle = 40f;
	private float rotationY = 0f;
	private float rotationX = 0f;

	// Focus
    [SerializeField]
	private float MaxFocusDistance = 4f;
	private float focus_lerp;

	// First person
	private float first_person_rotationY = 0f;
	private float first_person_rotationX = 0f;

    // Speaking
    private int side = 0;
    private float speak_lerp;

	/// <summary>
	/// Gets the state of the camera.
	/// </summary>
	/// <value>The state of the camera.</value>
	public CameraState CameraState { get { return camera_state; } }

	/// <summary>
	/// Gets the focus target.
	/// </summary>
	/// <value>The focus target.</value>
	public GameObject FocusTarget { get { return focus_target; } }

    /// <summary>
    /// Enable
    /// </summary>
    void OnEnable()
    {
        fov = NormalFOV;
    }

    /// <summary>
    /// Awake
    /// </summary>
	void Awake()
    {
		a = GetComponent<Animator>();
        cam = Camera.main;
		motor = GetComponent<PlayerMotor>();
        //hud = FindObjectOfType<menu_in_game_hud>();
        hud = GameObjectTools.GetComponentInChildren<menu_in_game_hud>(FindObjectOfType<menu_in_game>().gameObject);
    }

	/// <summary>
	/// Initialize player camera.
	/// </summary>
	public override void Start ()
    {
        base.Start();
        // Camera objects
		camera_default_target_object = new CameraObject("Default Target", new Vector3(0f, 2.5f, 0f), new GameObject().transform, transform, new Vector3(0f, 180f, 0f));
		camera_target_object = new CameraObject("Camera Target", new Vector3(0f, 1.5f, 0f), new GameObject().transform, null, Vector3.zero);
		camera_mount_point = new CameraObject("Camera Mount", normal_camera_position, new GameObject().transform, camera_target_object.XForm, Vector3.zero);
        // Parent camera
        cam.transform.parent = camera_target_object.XForm.parent;
	}
	
	/// <summary>
	/// Main update cycle.
	/// </summary>
	void Update ()
    {        
        // Position camera
        camera_target_object.XForm.position = transform.position + new Vector3(0f, 1.5f, 0f);

        // ######################################################################################################## TEMP
        if (camera_target_object.XForm.position.y < 0f)
        {
            camera_target_object.XForm.position = new Vector3(camera_target_object.XForm.position.x, 0f, camera_target_object.XForm.position.z);
            cam.transform.LookAt(transform);
            return;
        }
        // ######################################################################################################## TEMP

        // Camera code
        switch (camera_state)
        {
            case CameraState.Normal:
                if (!Paused)
                {
                    NormalCamera();
                    AllowTargeting();
                    AllowFirstPerson();
                }                    
                break;
            case CameraState.Target:
                if (!Paused)
                {
                    Targeting();
                    AllowTargeting();
                }                    
                break;
            case CameraState.FirstPerson:
                if (!Paused)
                {
                    FirstPerson();
                    AllowFirstPerson();
                }                    
                break;
            case CameraState.Speaking:
                Speaking();
                break;
        }

        RepositionCamera();
    }

	/// <summary>
	/// Lates the update.
	/// </summary>
	void LateUpdate()
    {
        if (do_late_hip_rotation && motor.IsWeaponEquiped) AimShield();
	}

	/// <summary>
	/// Transforms the direction.
	/// </summary>
	/// <returns>The direction.</returns>
	/// <param name="Direction">Direction.</param>
	public Vector3 TransformDirection(Vector3 Direction)
    {
        return cam.transform.TransformDirection(Direction);
    }

    // ####################################################################################################################################### Camera Collision

    /// <summary>
    /// Check for collisions between character and camera and move camera ( call in late update )
    /// </summary>
    private void RepositionCamera()
    {
        bool success; RaycastHit hit; Vector3 start;
        start = transform.position + new Vector3(0f, 1.5f, 0f);
        success = Physics.Linecast(start, cam.transform.position, out hit, LayerMask.GetMask(Layers.Geometry));
        if (success)
        {
            cam.transform.position = hit.point;
        }
    }

    // ####################################################################################################################################### Normal

    /// <summary>
    /// Mechanic for normal camera.
    /// </summary>
    private void NormalCamera()
    {

		// Get input values taking invert into account
        if (Settings.CameraInvertY) rotationY += (Controls.Control.GetRV() * (Sensitivity * Settings.CameraSensitivity)) * Time.deltaTime;
        if (!Settings.CameraInvertY) rotationY -= (Controls.Control.GetRV() * (Sensitivity * Settings.CameraSensitivity)) * Time.deltaTime;
        rotationY = Mathf.Clamp(rotationY, MinimumAngle, MaximumAngle);
        if (Settings.CameraInvertX) rotationX -= (Controls.Control.GetRH() * (Sensitivity * Settings.CameraSensitivity)) * Time.deltaTime;
        if (!Settings.CameraInvertX) rotationX += (Controls.Control.GetRH() * (Sensitivity * Settings.CameraSensitivity)) * Time.deltaTime;
        
        // Rotate target object
        camera_target_object.XForm.localRotation = Quaternion.Euler(new Vector3(-rotationY, rotationX, 0f));
		
        // Field of View
		fov_lerp += fov_speed * Time.deltaTime;
		fov_lerp = Mathf.Clamp(fov_lerp, 0f, 1f);
        fov = Mathf.Lerp(fov, NormalFOV, fov_lerp);
        cam.fieldOfView = fov;
		
        // Animator
		focus_lerp -= 5f * Time.deltaTime;
		focus_lerp = Mathf.Clamp(focus_lerp, 0f, 1f);
		a.SetFloat(AnimatorConditions.Focus, focus_lerp);
		
        // Camera
		if (cam_lerp < 1f)
        {
            cam_lerp += cam_speed * Time.deltaTime;
            cam_lerp = Mathf.Clamp(cam_lerp, 0f, 1f);
            cam.transform.position = Vector3.Lerp(cam.transform.position, camera_mount_point.XForm.position, cam_lerp);
		}
        else
        {
			cam.transform.position = camera_mount_point.XForm.position;
		}
		cam.transform.LookAt(camera_target_object.XForm);
	}

    // ####################################################################################################################################### Targeting

    /// <summary>
    /// Allow targeting
    /// </summary>
    private void AllowTargeting()
    {
        if (Controls.Control.GetLT() > 0.5f)
        {
            ToggleTargeting(true);
        }
        else
        {
            ToggleTargeting(false);
        }
    }

    /// <summary>
	/// Switch between targeting and normal camera.
	/// </summary>
	/// <param name="state">If set to <c>true</c> state.</param>
	private void ToggleTargeting(bool ZoomIn)
    {
        if (ZoomIn != is_zoomed_in)
        {
            if (ZoomIn)
            {
                camera_state = CameraState.Target;
                hip_rotationX = 0f;
                hip_rotationY = 0f;                
                AudioSource.PlayClipAtPoint(Sounds.CameraZoomIn, transform.position, Settings.SoundVolume);
                FindTarget();
                if (focus_target == null) do_late_hip_rotation = true;
            }
            else
            {
                camera_state = CameraState.Normal;                
                focus_target = null;
                do_late_hip_rotation = false;
                AudioSource.PlayClipAtPoint(Sounds.CameraZoomOut, transform.position, Settings.SoundVolume);
            }
            fov_lerp = 0f;
            is_zoomed_in = ZoomIn;
        }
    }

    /// <summary>
    /// Mechanic when Targeting.
    /// </summary>
    private void Targeting() {

		// Rotate towards character direction
		camera_target_object.XForm.localRotation = 
			Quaternion.RotateTowards(camera_target_object.XForm.localRotation, camera_default_target_object.XForm.rotation, cam_target_speed * Time.deltaTime);

        // Apply rotation to input
		rotationY = camera_target_object.XForm.localRotation.eulerAngles.x;
		rotationX = camera_target_object.XForm.localRotation.eulerAngles.y;

		// FOV
		//float fov_step = 10f * Time.deltaTime;
		fov_lerp += fov_speed * Time.deltaTime;
		fov_lerp = Mathf.Clamp(fov_lerp, 0f, 1f);
        fov = Mathf.Lerp(fov, ZoomFOV, fov_lerp);
        cam.fieldOfView = fov;

        // Animator
        focus_lerp += 5f * Time.deltaTime;
		focus_lerp = Mathf.Clamp(focus_lerp, 0f, 1f);
		a.SetFloat(AnimatorConditions.Focus, focus_lerp);

		// Camera
		cam.transform.position = camera_mount_point.XForm.position;
		cam.transform.LookAt(camera_target_object.XForm);

		// Focus
		if (focus_target != null) {

            // Rotate towards target
			Vector3 focus_vector = focus_target.transform.position;
			focus_vector.y = transform.position.y;
			transform.LookAt(focus_vector);

			// Check distance
			float dist = Vector3.Distance(focus_target.transform.position, transform.position);
			if (dist > MaxFocusDistance) ToggleTargeting(false);

		}
	}

    /// <summary>
	/// Find nearest targetable object.
	/// </summary>
	private void FindTarget()
    {
        Targetable nearest = null;
        float nearest_dist = Mathf.Infinity;
        // Get all targetable objects in the scene
        Targetable[] focus_targets = FindObjectsOfType<Targetable>();
        // Find nearest targetable object
        foreach (Targetable ft in focus_targets)
        {
            float dist = Vector3.Distance(ft.gameObject.transform.position, transform.position);
            if (dist <= MaxFocusDistance && dist < nearest_dist)
            {
                nearest_dist = dist;
                nearest = ft;
            }
        }
        // Check distance
        if (nearest != null)
        {
            focus_target = nearest.gameObject;
        }
    }

    // ####################################################################################################################################### Aim Shield

    /// <summary>
    /// Rotate character bones to aim with shield ( call in late update )
    /// </summary>
    private void AimShield()
    {
        // Get input values taking sensitivity into account
		hip_rotationY -= (Controls.Control.GetRV() * Sensitivity) * Time.deltaTime;
		hip_rotationY = Mathf.Clamp(hip_rotationY, -HipMaxAngleY, HipMaxAngleY);
		hip_rotationX += (Controls.Control.GetRH() * Sensitivity) * Time.deltaTime;
		hip_rotationX = Mathf.Clamp(hip_rotationX, -HipMaxAngleX, HipMaxAngleX);
        // Rotate bones
        float rotationX = hip_rotationX / aim_bones.Length;
        float rotationY = hip_rotationY / aim_bones.Length;
        for (int i = 0; i < aim_bones.Length; i++)
        {
            aim_bones[i].transform.eulerAngles += new Vector3(rotationY, rotationX, 0f);
        }
        // Counter rotate bones
        for (int i = 0; i < counter_aim_bones.Length; i++)
        {
            counter_aim_bones[i].transform.eulerAngles -= new Vector3(rotationY, rotationX, 0f);
        }
    }

    // ####################################################################################################################################### First Person

    /// <summary>
    /// Allow toggle to first person
    /// </summary>
    private void AllowFirstPerson()
    {
        // Input for first person view
        if (Controls.Control.GetRDown()) { ToggleFirstPerson(); }
    }

    /// <summary>
    /// Toggle first person view
    /// </summary>
    private void ToggleFirstPerson()
    {
        if (camera_state == CameraState.Normal)
        {
            camera_state = CameraState.FirstPerson;
            hud.HideButtons(true);
            hud.SwitchView(true);
            cam_lerp = 0f;
            AudioSource.PlayClipAtPoint(Sounds.CameraZoomIn, transform.position, Settings.SoundVolume);
        }
        else if (camera_state == CameraState.FirstPerson)
        {
            camera_state = CameraState.Normal;
            hud.HideButtons(false);
            hud.SwitchView(false);
            cam_lerp = 0f;
            rotationY = camera_default_target_object.XForm.rotation.eulerAngles.x;
            rotationX = camera_default_target_object.XForm.rotation.eulerAngles.y;
            AudioSource.PlayClipAtPoint(Sounds.CameraZoomOut, transform.position, Settings.SoundVolume);
        }
    }

    /// <summary>
    /// First person camera code
    /// </summary>
    private void FirstPerson()
    {

        // Transition to first person
		if (cam_lerp < 1f)
        {
            cam_lerp += cam_speed * Time.deltaTime;
			cam.transform.rotation = Quaternion.Lerp(cam.transform.rotation, transform.rotation, cam_lerp);
            first_person_rotationY = -cam.transform.rotation.eulerAngles.x;
            first_person_rotationX = cam.transform.rotation.eulerAngles.y;
        }
        else
        {
            // Rotate character
            transform.rotation = Quaternion.Euler(new Vector3(0f, first_person_rotationX, 0f));
        }

        // Position
        cam.transform.position = Vector3.Lerp(cam.transform.position, transform.position + (transform.up * 1.25f), cam_lerp);

		// Get input values taking invert and sensitivity into account
        if (Settings.CameraFPInvertY) first_person_rotationY += (Controls.Control.GetRV() * (Sensitivity * Settings.CameraFPSensitivity)) * Time.deltaTime;
        if (!Settings.CameraFPInvertY) first_person_rotationY -= (Controls.Control.GetRV() * (Sensitivity * Settings.CameraFPSensitivity)) * Time.deltaTime;
        first_person_rotationY = Mathf.Clamp(first_person_rotationY, MinimumAngle, MaximumAngle);
        if (Settings.CameraFPInvertX) first_person_rotationX -= (Controls.Control.GetRH() * (Sensitivity * Settings.CameraFPSensitivity)) * Time.deltaTime;
        if (!Settings.CameraFPInvertX) first_person_rotationX += (Controls.Control.GetRH() * (Sensitivity * Settings.CameraFPSensitivity)) * Time.deltaTime;

        // Rotate camera
        cam.transform.rotation = Quaternion.Euler(new Vector3(-first_person_rotationY, first_person_rotationX, 0f));
                    
	}

    // ####################################################################################################################################### Speaking

    /// <summary>
    /// Toggle Speaking
    /// </summary>
    public void ToggleSpeaking()
    {
        if (camera_state != CameraState.Speaking)
        {
            camera_state = CameraState.Speaking;
            side = Random.Range(0, 2);
            speak_lerp = 0f;
        }        
        else
        {
            camera_state = CameraState.Normal;
            is_zoomed_in = false;
            cam_lerp = 0f;
            focus_target = null;
        }
    }

    /// <summary>
    /// Speaking camera code
    /// </summary>
    private void Speaking()
    {
        Vector3 cam_pos;
        if (side == 0)
        {
            cam_pos = transform.position + (transform.right * 0.5f) + (-transform.forward * 0.75f) + (transform.up * 1.2f);
        }        
        else
        {
            cam_pos = transform.position + (-transform.right * 0.5f) + (-transform.forward * 0.75f) + (transform.up * 1.2f);
        }
        if (speak_lerp < 1f)
        {
            speak_lerp += Time.deltaTime;
        }
        cam.transform.position = Vector3.Lerp(cam.transform.position, cam_pos, speak_lerp);
        cam.transform.LookAt(focus_target.transform);
    }

}
