﻿using UnityEngine;
using System.Collections;

public class Sounds : MonoBehaviour {

	/// <summary>
	/// The menu_navigate.
	/// </summary>
	private static AudioClip menu_navigate = Resources.Load("Sounds/menu_navigate", typeof(AudioClip)) as AudioClip;
	public static AudioClip MenuNavigate { get { return menu_navigate; } }

	private static AudioClip menu_finish = Resources.Load("Sounds/menu_finish", typeof(AudioClip)) as AudioClip;
	public static AudioClip MenuFinish { get { return menu_finish; } }

	private static AudioClip menu_cancel = Resources.Load("Sounds/menu_cancel", typeof(AudioClip)) as AudioClip;
	public static AudioClip MenuCancel { get { return menu_cancel; } }

	private static AudioClip menu_click = Resources.Load("Sounds/menu_click", typeof(AudioClip)) as AudioClip;
	public static AudioClip MenuClick { get { return menu_click; } }

	private static AudioClip menu_load = Resources.Load("Sounds/menu_load", typeof(AudioClip)) as AudioClip;
	public static AudioClip MenuLoad { get { return menu_load; } }

	/// <summary>
	/// The footstep.
	/// </summary>
	private static AudioClip footstep = Resources.Load("Sounds/foot_step", typeof(AudioClip)) as AudioClip;
	public static AudioClip FootStep { get { return footstep; } }

	private static AudioClip footjump = Resources.Load("Sounds/foot_jump", typeof(AudioClip)) as AudioClip;
	public static AudioClip FootJump { get { return footjump; } }

	/// <summary>
	/// The camera_zoom_in.
	/// </summary>
	private static AudioClip camera_zoom_in = Resources.Load("Sounds/camera_zoom_in", typeof(AudioClip)) as AudioClip;
	public static AudioClip CameraZoomIn { get { return camera_zoom_in; } }

	private static AudioClip camera_zoom_out = Resources.Load("Sounds/camera_zoom_out", typeof(AudioClip)) as AudioClip;
	public static AudioClip CameraZoomOut { get { return camera_zoom_out; } }

	/// <summary>
	/// The roll.
	/// </summary>
	private static AudioClip roll = Resources.Load("Sounds/roll", typeof(AudioClip)) as AudioClip;
	public static AudioClip Roll { get { return roll; } }

	/// <summary>
	/// sword draw sounds.
	/// </summary>
	private static AudioClip sword_draw = Resources.Load("Sounds/sword_draw", typeof(AudioClip)) as AudioClip;
	public static AudioClip SwordDraw { get { return sword_draw; } }

	private static AudioClip sword_sheath = Resources.Load("Sounds/sword_sheath", typeof(AudioClip)) as AudioClip;
	public static AudioClip SwordSheath { get { return sword_sheath; } }

	private static AudioClip sword_swing_1 = Resources.Load("Sounds/sword_swing_1", typeof(AudioClip)) as AudioClip;
	public static AudioClip SwordSwing1 { get { return sword_swing_1; } }

	private static AudioClip sword_repel_1 = Resources.Load("Sounds/sword_repel_1", typeof(AudioClip)) as AudioClip;
	public static AudioClip SwordRepel1 { get { return sword_repel_1; } }

	/// <summary>
	/// sword_walk sounds.
	/// </summary>
	private static AudioClip sword_walk_1 = Resources.Load("Sounds/sword_walk_1", typeof(AudioClip)) as AudioClip;
	public static AudioClip SwordWalk1 { get { return sword_walk_1; } }

	private static AudioClip sword_walk_2 = Resources.Load("Sounds/sword_walk_2", typeof(AudioClip)) as AudioClip;
	public static AudioClip SwordWalk2 { get { return sword_walk_2; } }

	private static AudioClip sword_walk_3 = Resources.Load("Sounds/sword_walk_3", typeof(AudioClip)) as AudioClip;
	public static AudioClip SwordWalk3 { get { return sword_walk_3; } }

	private static AudioClip sword_walk_4 = Resources.Load("Sounds/sword_walk_4", typeof(AudioClip)) as AudioClip;
	public static AudioClip SwordWalk4 { get { return sword_walk_4; } }

    /// <summary>
    /// Conversation sounds
    /// </summary>
    private static AudioClip conversation_next = Resources.Load("Sounds/conversation_next", typeof(AudioClip)) as AudioClip;
    public static AudioClip ConversationNext { get { return conversation_next; } }

    private static AudioClip conversation_finish = Resources.Load("Sounds/conversation_finish", typeof(AudioClip)) as AudioClip;
    public static AudioClip ConversationFinish { get { return conversation_finish; } }

}
