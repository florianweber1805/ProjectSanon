﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

public class SaveGame {

	public const string DefaultName = "Sanon";
	public const int DefaultHearts = 3;
	private const string Folder = "Sanon";
	private static string SavePath {
		get {
			string appdata = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Sanon";
			if (!Directory.Exists(appdata)) {
				Directory.CreateDirectory(appdata);
			}
//			Debug.Log(appdata);
			return appdata + "\\Save%N%.xml";
		}
	}

	public int ID;
	public string Name;
	// Hearts
	public int Hearts;
	public int HeartPieces;

	public SaveGame() {
	}

	public SaveGame(int NewID) {
		ID = NewID;
		Name = DefaultName;
		Hearts = DefaultHearts;
	}

	public static void Test() {
		SaveGame s = new SaveGame(1);
		s.Save();
	}

	public static void TestB() {
		SaveGame s = SaveGame.Load(1);
		Debug.Log(s.Name);
	}

	public static List<SaveGame> All() {
		List<SaveGame> saves = new List<SaveGame>();
		for (int i = 1; i <= 3; i++) {
			string path = SavePath.Replace("%N%", i.ToString());
			if (File.Exists(path)) {
				saves.Add(Load(i));
			} else {
				saves.Add(new SaveGame());
			}
		}
		return saves;
	}

	public static SaveGame Load(int ID) {
		SaveGame save = new SaveGame();
		string path = SavePath.Replace("%N%", ID.ToString());
		if (File.Exists(path)) {
			try {
				XmlSerializer ser = new XmlSerializer(typeof(SaveGame));
				FileStream stream = new FileStream(path, FileMode.Open);
				save = ser.Deserialize(stream) as SaveGame;
				stream.Close();
			} catch {
			}
		}
		return save;
	}

	public void Save() {
		string path = SavePath.Replace("%N%", ID.ToString());
		XmlSerializer serializer = new XmlSerializer(typeof(SaveGame));
		Encoding encoding = Encoding.GetEncoding("UTF-8");		
		using(StreamWriter stream = new StreamWriter(path, false, encoding)) {
			serializer.Serialize(stream, this);
		}

	}

	public void Delete() {
		string path = SavePath.Replace("%N%", ID.ToString());
		if (File.Exists(path)) {
			File.Delete(path);
		}
	}

}