﻿using UnityEngine;
using System.Collections;

public class menu_shrinkle : MonoBehaviour {

	public float MaxOffset = 0.2f;
	public float Step = 1f;

	private RectTransform rec;
	private bool direction = false;

	// Use this for initialization
	void Start () {
		rec = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 new_scale = rec.localScale;
		// Change
		if (direction) {
			if (new_scale.x - 1f < MaxOffset) {
				new_scale.x += Step * Time.deltaTime;
				new_scale.y += Step * Time.deltaTime;
			} else {
				direction = !direction;
			}
		} else {
			if (new_scale.x - 1f > MaxOffset / 2f) {
				new_scale.x -= Step * Time.deltaTime;
				new_scale.y -= Step * Time.deltaTime;
			} else {
				direction = !direction;
			}
		}
		// Clamp
		new_scale.x = Mathf.Clamp(new_scale.x, 1f, 1f + MaxOffset);
		new_scale.y = Mathf.Clamp(new_scale.y, 1f, 1f + MaxOffset);
		// Set scale
		rec.localScale = new_scale;
	}

}
