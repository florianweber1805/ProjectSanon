﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_main : MonoBehaviour {

	// State
	public enum menu_main_state { PressA, Buttons, SaveGame, NewGame, LoadGame, DeleteGame, Options };
    public enum menu_sound { None, Click, Navigate, Cancel, Load, Finish };
	// Panels
	public menu_main_title menu_main_title;
	public menu_main_press menu_main_press;
	public menu_main_buttons menu_main_buttons;
	public menu_main_savegame menu_main_savegame;
	public menu_main_new_game menu_main_new_game;
	public menu_main_load_save_game menu_main_load_save_game;
	public menu_main_side_buttons menu_main_side_buttons;
	public menu_main_delete_save_game menu_main_delete_save_game;
	public menu_main_options menu_main_options;

	void Start() {
		Navigate(menu_main_state.PressA);
	}

	public void Navigate(menu_main_state Menu, menu_sound Sound = menu_sound.None) {
        // Sound
        switch (Sound)
        {
            case menu_sound.Navigate:
                AudioSource.PlayClipAtPoint(Sounds.MenuNavigate, Camera.main.transform.position, Settings.SoundVolume);
                break;
            case menu_sound.Click:
                AudioSource.PlayClipAtPoint(Sounds.MenuClick, Camera.main.transform.position, Settings.SoundVolume);
                break;
            case menu_sound.Cancel:
                AudioSource.PlayClipAtPoint(Sounds.MenuCancel, Camera.main.transform.position, Settings.SoundVolume);
                break;
            case menu_sound.Finish:
                AudioSource.PlayClipAtPoint(Sounds.MenuFinish, Camera.main.transform.position, Settings.SoundVolume);
                break;
            case menu_sound.Load:
                AudioSource.PlayClipAtPoint(Sounds.MenuLoad, Camera.main.transform.position, Settings.SoundVolume);
                break;
        }
		// Deactivate all
		menu_main_title.gameObject.SetActive(false);
		menu_main_press.gameObject.SetActive(false);
		menu_main_buttons.gameObject.SetActive(false);
		menu_main_savegame.gameObject.SetActive(false);
		menu_main_new_game.gameObject.SetActive(false);
		menu_main_load_save_game.gameObject.SetActive(false);
		menu_main_side_buttons.gameObject.SetActive(false);
		menu_main_delete_save_game.gameObject.SetActive(false);
		menu_main_options.gameObject.SetActive(false);
		// Show active
		switch (Menu) {
		case menu_main_state.PressA:
			menu_main_title.gameObject.SetActive(true);
			menu_main_press.gameObject.SetActive(true);
			break;
		case menu_main_state.Buttons:
			menu_main_title.gameObject.SetActive(true);
			menu_main_buttons.gameObject.SetActive(true);
			menu_main_side_buttons.gameObject.SetActive(true);
			break;
		case menu_main_state.SaveGame:
			menu_main_savegame.gameObject.SetActive(true);
			menu_main_side_buttons.gameObject.SetActive(true);
			break;
		case menu_main_state.NewGame:
			menu_main_new_game.gameObject.SetActive(true);
			menu_main_side_buttons.gameObject.SetActive(true);
			break;
		case menu_main_state.LoadGame:
			menu_main_load_save_game.gameObject.SetActive(true);
			menu_main_side_buttons.gameObject.SetActive(true);
			break;
		case menu_main_state.DeleteGame:
			menu_main_delete_save_game.gameObject.SetActive(true);
			menu_main_side_buttons.gameObject.SetActive(true);
			break;
		case menu_main_state.Options:
			menu_main_options.gameObject.SetActive(true);
                menu_main_side_buttons.gameObject.SetActive(true);
                break;
		}
	}
	
}
