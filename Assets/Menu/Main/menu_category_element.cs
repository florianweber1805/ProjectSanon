﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_category_element : MonoBehaviour {

	public float InputStrength = 0.5f;
	public float InputWait = 0.25f;
	public GameObject highlight_frame;
	public GameObject select_left;
	public GameObject select_right;
	public GameObject panel;

	private bool selected;
	private float initial_wait;
	
	void OnEnable() {
		selected = false;
	}
	
	void Update() {
		if (initial_wait <= 0f) {
			if (selected) Navigate();
		} else initial_wait -= Time.deltaTime;
	}
	
	void Navigate() {
		if (Controls.Control.GetLBDown()) StartCoroutine(Select(select_left));
		if (Controls.Control.GetRBDown()) StartCoroutine(Select(select_right));
	}
	
	IEnumerator Select(GameObject ToSelect) {
		if (ToSelect) {
            if (panel) panel.SetActive(false);
            menu_category_element mce = ToSelect.GetComponent<menu_category_element>();
			mce.OnActivate();
			AudioSource.PlayClipAtPoint(Sounds.MenuClick, Camera.main.transform.position, Settings.SoundVolume);
			selected = false;
			yield return new WaitForSeconds(InputWait);
		}
	}

	public void OnActivate() {
		RectTransform frame_rec = highlight_frame.GetComponent<RectTransform>();
		RectTransform rec = gameObject.GetComponent<RectTransform>();
		frame_rec.position = rec.position;
		frame_rec.sizeDelta = rec.sizeDelta;
		initial_wait = InputWait;
		selected = true;
        if (panel) panel.SetActive(true);
	}

}
