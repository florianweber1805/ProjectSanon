﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class menu_main_options_display : MonoBehaviour {

    public GameObject SelectFix;
    public GameObject SelectFirst;

    public Toggle FullscreenToggle;
    public menu_main_options_resolution_changer ResolutionChanger;

    void OnEnable()
    {
        // Values
        FullscreenToggle.isOn = Screen.fullScreen;
        // Select
        StartCoroutine(DoSelect());
    }

    IEnumerator DoSelect()
    {
        EventSystem es = FindObjectOfType<EventSystem>();
        yield return new WaitForFixedUpdate();
        es.SetSelectedGameObject(SelectFix);
        yield return new WaitForFixedUpdate();
        es.SetSelectedGameObject(SelectFirst);
    }

    public void Accept()
    {
        //StartCoroutine(ChangeResolution());
        Screen.SetResolution(ResolutionChanger.Resolution.width, ResolutionChanger.Resolution.height, FullscreenToggle.isOn, ResolutionChanger.Resolution.refreshRate);
    }

    //IEnumerator ChangeResolution()
    //{
    //    yield return new WaitForEndOfFrame();

    //    Resolution smallest = Screen.resolutions[0];
    //    Screen.SetResolution(smallest.width, smallest.height, Screen.fullScreen, smallest.refreshRate);

    //    yield return new WaitForEndOfFrame();

    //    Screen.SetResolution(ResolutionChanger.Resolution.width, ResolutionChanger.Resolution.height, Screen.fullScreen, ResolutionChanger.Resolution.refreshRate);

    //    yield return new WaitForEndOfFrame();            

    //    Screen.fullScreen = !FullscreenToggle.isOn;

    //    yield return new WaitForEndOfFrame();

    //    Screen.fullScreen = FullscreenToggle.isOn;
    //}

}
