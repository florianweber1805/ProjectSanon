﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_main_options_audio : MonoBehaviour {

    public GameObject SelectFix;
    public GameObject SelectFirst;

    public Slider master;
    public Slider sounds;
    public Slider music;

    void Start()
    {
        master.onValueChanged.AddListener(ChangeMaster);
        sounds.onValueChanged.AddListener(ChangeSounds);
        music.onValueChanged.AddListener(ChangeMusic);
    }

    void OnEnable()
    {
        // Values
        master.value = Settings.VolumeMaster;
        sounds.value = Settings.VolumeSounds;
        music.value = Settings.VolumeMusic;
        // Select
        StartCoroutine(DoSelect());
    }

    IEnumerator DoSelect()
    {
        EventSystem es = FindObjectOfType<EventSystem>();
        yield return new WaitForFixedUpdate();
        es.SetSelectedGameObject(SelectFix);
        yield return new WaitForFixedUpdate();
        es.SetSelectedGameObject(SelectFirst);
    }

    void OnDisable()
    {
        Settings.Save();
    }

    /// <summary>
    /// Change master volume
    /// </summary>
    /// <param name="value"></param>
    void ChangeMaster(float value)
    {
        Settings.VolumeMaster = value;
    }

    /// <summary>
    /// Change sound volume
    /// </summary>
    /// <param name="value"></param>
    void ChangeSounds(float value)
    {
        Settings.VolumeSounds = value;
    }

    /// <summary>
    /// Change music volume
    /// </summary>
    /// <param name="value"></param>
    void ChangeMusic(float value)
    {
        Settings.VolumeMusic = value;
    }

}
