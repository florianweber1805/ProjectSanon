﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_main_options_resolution_changer : MonoBehaviour {

    public float InputStrength = 0.5f;
    public float InputWait = 0.25f;

    private Text label;
    private Resolution[] resolutions;
    private EventSystem es;
    private int index;
    private float input_timer;
    private Resolution selected;

    public Resolution Resolution
    {
        get { return selected; }
    }

    void Awake()
    {
        es = FindObjectOfType<EventSystem>();
        resolutions = Screen.resolutions;
        label = GetComponentInChildren<Text>();        
    }

    void OnEnable()
    {
        selected = GetResolution();
        label.text = Screen.width.ToString() + "x" + Screen.height.ToString() + " " + Screen.currentResolution.refreshRate.ToString() + " Hz";
    }

    Resolution GetResolution()
    {
        Resolution resolution = new Resolution();
        resolution.width = Screen.width;
        resolution.height = Screen.height;
        resolution.refreshRate = Screen.currentResolution.refreshRate;
        // Get index
        for (int i = 0; i < resolutions.Length - 1; i++)
        {
            if (resolutions[i].width == resolution.width && resolutions[i].height == resolution.height)
            {
                index = i;
            }
        }
        // Return
        return resolution;
    }

    void Update()
    {
        bool change = false;
        if (es.currentSelectedGameObject == gameObject)
        {
            if (input_timer <= 0f)
            {
                if (Controls.Control.GetLH() > InputStrength)
                {
                    if (index == resolutions.Length - 1)
                    {
                        index = 0;
                    }
                    else
                    {
                        index++;
                    }
                    input_timer = InputWait;
                    change = true;
                }
                else if (Controls.Control.GetLH() < -InputStrength)
                {
                    if (index == 0)
                    {
                        index = resolutions.Length - 1;
                    }
                    else
                    {
                        index--;
                    }
                    input_timer = InputWait;
                    change = true;
                }
                if (change)
                {
                    selected = resolutions[index];
                    label.text = resolutions[index].width.ToString() + "x" + resolutions[index].height.ToString() + " " + resolutions[index].refreshRate.ToString() + " Hz";
                    AudioSource.PlayClipAtPoint(Sounds.MenuNavigate, Camera.main.transform.position, Settings.SoundVolume);
                }                
            }            
            else
            {
                input_timer -= Time.deltaTime;
            }
        }        
    }

}
