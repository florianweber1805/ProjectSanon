﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_main_options_game : MonoBehaviour {

    public GameObject SelectFix;
    public GameObject SelectFirst;

    public Slider TextSpeed;

    void Start()
    {
        TextSpeed.onValueChanged.AddListener(ChangeTextSpeed);
    }

    void OnEnable()
    {
        // Values
        TextSpeed.value = Settings.TextSpeed;
        // Select
        StartCoroutine(DoSelect());
    }

    IEnumerator DoSelect()
    {
        EventSystem es = FindObjectOfType<EventSystem>();
        //yield return new WaitForFixedUpdate();
        //es.SetSelectedGameObject(SelectFix);
        yield return new WaitForFixedUpdate();
        es.SetSelectedGameObject(SelectFirst);
    }

    void ChangeTextSpeed(float value)
    {
        Settings.TextSpeed = value;
    }

    void OnDisable()
    {
        Settings.Save();
    }

}
