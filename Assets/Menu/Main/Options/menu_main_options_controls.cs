﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_main_options_controls : MonoBehaviour {

    public GameObject SelectFix;
    public GameObject SelectFirst;

    public Toggle CameraXInvert;
    public Toggle CameraYInvert;
    public Slider CameraSensitivity;
    public Toggle CameraFPXInvert;
    public Toggle CameraFPYInvert;
    public Slider CameraFPSensitivity;

    void Start()
    {
        CameraXInvert.onValueChanged.AddListener(ChangeCameraXInvert);
        CameraYInvert.onValueChanged.AddListener(ChangeCameraYInvert);
        CameraSensitivity.onValueChanged.AddListener(ChangeCameraSensitivity);
        CameraFPXInvert.onValueChanged.AddListener(ChangeCameraFPXInvert);
        CameraFPYInvert.onValueChanged.AddListener(ChangeCameraFPYInvert);
        CameraFPSensitivity.onValueChanged.AddListener(ChangeCameraFPSensitivity);
    }

    void OnEnable()
    {
        // Values
        CameraXInvert.isOn = Settings.CameraInvertX;
        CameraYInvert.isOn = Settings.CameraInvertY;
        CameraSensitivity.value = Settings.CameraSensitivity;
        CameraFPXInvert.isOn = Settings.CameraFPInvertX;
        CameraFPYInvert.isOn = Settings.CameraFPInvertY;
        CameraFPSensitivity.value = Settings.CameraFPSensitivity;
        // Select
        StartCoroutine(DoSelect());
    }

    IEnumerator DoSelect()
    {
        EventSystem es = FindObjectOfType<EventSystem>();
        yield return new WaitForFixedUpdate();
        es.SetSelectedGameObject(SelectFix);
        yield return new WaitForFixedUpdate();
        es.SetSelectedGameObject(SelectFirst);
    }

    void OnDisable()
    {
        Settings.Save();
    }

    /// <summary>
    /// Change camera x invert
    /// </summary>
    /// <param name="value"></param>
    void ChangeCameraXInvert(bool value)
    {
        Settings.CameraInvertX = value;
    }

    /// <summary>
    /// Change camera y invert
    /// </summary>
    /// <param name="value"></param>
    void ChangeCameraYInvert(bool value)
    {
        Settings.CameraInvertY = value;
    }

    /// <summary>
    /// Change camera sensitivity
    /// </summary>
    /// <param name="value"></param>
    void ChangeCameraSensitivity(float value)
    {
        Settings.CameraSensitivity = value;
    }

    /// <summary>
    /// Change first person camera x invert
    /// </summary>
    /// <param name="value"></param>
    void ChangeCameraFPXInvert(bool value)
    {
        Settings.CameraFPInvertX = value;
    }

    /// <summary>
    /// Change first person camera y invert
    /// </summary>
    /// <param name="value"></param>
    void ChangeCameraFPYInvert(bool value)
    {
        Settings.CameraFPInvertY = value;
    }

    /// <summary>
    /// Change first person sensitivity
    /// </summary>
    /// <param name="value"></param>
    void ChangeCameraFPSensitivity(float value)
    {
        Settings.CameraFPSensitivity = value;
    }

}
