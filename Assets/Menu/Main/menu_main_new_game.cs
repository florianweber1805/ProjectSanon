﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_main_new_game : MonoBehaviour {

	public GameObject SelectFirst;
	public GameObject SelectFinish;
	public menu_main_savegame mms;
	public Text txt_name;
	private menu_main mm;
	
	void Start() {
		mm = FindObjectOfType<menu_main>();
	}

	void OnEnable() {
        // Values
		txt_name.text = SaveGame.DefaultName;
        // Select
        StartCoroutine(SelectFirstElement());
	}

	void Update() {
		if (Controls.Control.GetBDown()) DeleteLetterOrBack(true);
		if (Controls.Control.GetStartDown()) SelectFinishElement();
	}

    /// <summary>
    /// Delete letter or go back
    /// </summary>
    /// <param name="AllowBack"></param>
	public void DeleteLetterOrBack(bool AllowBack = false) {
		if (txt_name.text.Length > 0) {
			txt_name.text = txt_name.text.Substring(0, txt_name.text.Length-1);
			AudioSource.PlayClipAtPoint(Sounds.MenuCancel, Camera.main.transform.position, Settings.SoundVolume);
		} else if (AllowBack) {
			Back();
		}
	}

    /// <summary>
    /// Select finish element
    /// </summary>
	void SelectFinishElement() {
		EventSystem es = FindObjectOfType<EventSystem>();
		es.SetSelectedGameObject(SelectFinish);
		AudioSource.PlayClipAtPoint(Sounds.MenuClick, Camera.main.transform.position, Settings.SoundVolume);
	}

    /// <summary>
    /// Select first element
    /// </summary>
    /// <returns></returns>
	IEnumerator SelectFirstElement() {
        yield return new WaitForFixedUpdate();
		EventSystem es = FindObjectOfType<EventSystem>();
		es.SetSelectedGameObject(SelectFirst);
	}

    /// <summary>
    /// Press letter
    /// </summary>
    /// <param name="Letter"></param>
	public void PressLetter(string Letter) {
		if (txt_name.text.Length < 8) {
			txt_name.text = txt_name.text + Letter;
			AudioSource.PlayClipAtPoint(Sounds.MenuClick, Camera.main.transform.position, Settings.SoundVolume);
		} else AudioSource.PlayClipAtPoint(Sounds.MenuCancel, Camera.main.transform.position, Settings.SoundVolume);
	}

    /// <summary>
    /// Go back
    /// </summary>
	void Back() {
		mm.Navigate(menu_main.menu_main_state.SaveGame, menu_main.menu_sound.Cancel);
	}

    /// <summary>
    /// Press done
    /// </summary>
	public void Done() {
		SaveGame save = new SaveGame(mms.SelectedID);
		save.Name = txt_name.text;
		save.Save();
		mm.Navigate(menu_main.menu_main_state.SaveGame, menu_main.menu_sound.Finish);
	}

}
