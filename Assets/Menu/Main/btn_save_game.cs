﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class btn_save_game : MonoBehaviour {
	
	public Text lbl_name;
	public GameObject pnl_hearts;
	public GameObject pnl_hearts_2;
	public GameObject icon_hearts;

	public void ShowHearts(int Count) {
		DeleteChilds(pnl_hearts);
		DeleteChilds(pnl_hearts_2);
		GameObject heart = null;
		// Cell size
//		AutoGridLayout agl = pnl_hearts.GetComponent<AutoGridLayout>();
//		agl.CalculateLayoutInputHorizontal();
		// Insert hearts
		for (int i = 1; i <= Count; i++) {
			heart = Instantiate<GameObject>(icon_hearts);
			if (i <= 10) heart.transform.SetParent(pnl_hearts.transform, false);
			else heart.transform.SetParent(pnl_hearts_2.transform, false);
		}
	}

	void DeleteChilds(GameObject obj) {
		List<GameObject> childs = new List<GameObject>();
		foreach (Transform child in obj.transform) {
			childs.Add(child.gameObject);
		}
		foreach (GameObject child in childs) {
			Destroy(child);
		}
	}

}
