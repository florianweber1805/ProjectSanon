﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Helper;

public class menu_main_load_save_game : MonoBehaviour {

	public GameObject SelectFirst;
	public menu_main_savegame mms;
	private menu_main mm;

	void Start() {
		mm = FindObjectOfType<menu_main>();
	}

    void OnEnable()
    {
        UpdateSaveGame();
        StartCoroutine(SelectFirstElement());
    }
	
	void Update() {
		if (Controls.Control.GetBDown()) Back();
	}

    /// <summary>
    /// Update selected savegame
    /// </summary>
	void UpdateSaveGame() {
		btn_save_game savegame = FindObjectOfType<btn_save_game>();
		savegame.lbl_name.text = mms.LoadedSaveGame.Name;
		savegame.ShowHearts(mms.LoadedSaveGame.Hearts);
	}

    /// <summary>
    /// Select first element
    /// </summary>
    /// <returns></returns>
	IEnumerator SelectFirstElement() {
        yield return new WaitForFixedUpdate();
		EventSystem es = FindObjectOfType<EventSystem>();
		es.SetSelectedGameObject(SelectFirst);
	}

    /// <summary>
    /// Press play
    /// </summary>
	public void Play() {
		StartCoroutine(DoPlay());
	}

	IEnumerator DoPlay() {
		AudioSource.PlayClipAtPoint(Sounds.MenuLoad, Camera.main.transform.position, Settings.SoundVolume);
		yield return new WaitForSeconds(Sounds.MenuLoad.length);
		Application.LoadLevel(Scenes.TestEnvironment);
	}
	
    /// <summary>
    /// Go back
    /// </summary>
	public void Back() {
		mm.Navigate(menu_main.menu_main_state.SaveGame, menu_main.menu_sound.Cancel);
	}

    /// <summary>
    /// Press delete
    /// </summary>
	public void Delete() {
		mm.Navigate(menu_main.menu_main_state.DeleteGame, menu_main.menu_sound.Click);
	}
	
}
