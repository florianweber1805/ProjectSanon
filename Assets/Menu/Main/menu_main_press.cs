﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class menu_main_press : MonoBehaviour {

	public GameObject SelectFirst;
	private menu_main mm;
	
	void Start() {
		mm = FindObjectOfType<menu_main>();
	}

    void OnEnable()
    {
        StartCoroutine(SelectFirstElement());
    }

    /// <summary>
    /// Select first element
    /// </summary>
	IEnumerator SelectFirstElement() {
        yield return new WaitForEndOfFrame();
		EventSystem es = FindObjectOfType<EventSystem>();
		es.SetSelectedGameObject(SelectFirst);
	}

    /// <summary>
    /// Press A
    /// </summary>
	public void PressA() {
		mm.Navigate(menu_main.menu_main_state.Buttons, menu_main.menu_sound.Finish);
	}

}
