﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_main_options : MonoBehaviour {

	private menu_main mm;

    public GameObject GamePanel;
    public GameObject AudioPanel;
    public GameObject DisplayPanel;
    public GameObject ControlPanel;

	public menu_category_element select_first_category;

	void Start() {
		mm = FindObjectOfType<menu_main>();
	}

    void OnEnable()
    {
        // Hide
        GamePanel.SetActive(false);
        AudioPanel.SetActive(false);
        DisplayPanel.SetActive(false);
        ControlPanel.SetActive(false);
        // Select
        StartCoroutine(SelectFirstCategoryElement());
    }

	void Update() {
		if (Controls.Control.GetBDown()) Back();
	}

    /// <summary>
    /// Select first category
    /// </summary>
    /// <returns></returns>
    IEnumerator SelectFirstCategoryElement()
    {
        yield return new WaitForFixedUpdate();
        select_first_category.OnActivate();
    }

    /// <summary>
    /// Go back
    /// </summary>
    void Back() {        
        mm.Navigate(menu_main.menu_main_state.PressA, menu_main.menu_sound.Cancel);
	}

}
