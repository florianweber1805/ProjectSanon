﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class menu_main_savegame : MonoBehaviour {

	public GameObject frame_highlight;
	public GameObject btn_new_game;
	public GameObject btn_save_game;
	public GameObject pnl_save_games;

	public SaveGame LoadedSaveGame;
	public int SelectedID;
	private menu_main mm;

	void Start() {
		mm = FindObjectOfType<menu_main>();
	}

    void OnEnable()
    {
        LoadSaveGames();
        StartCoroutine(SelectFirstElement());
    }

	void Update () {
		if (Controls.Control.GetBDown()) Back();
	}

    /// <summary>
    /// Get first child object / first savegame
    /// </summary>
    /// <returns></returns>
	GameObject FirstObject() {
		return pnl_save_games.transform.GetChild(0).gameObject;
	}

    /// <summary>
    /// Delete childs of a gameobject
    /// </summary>
    /// <param name="obj"></param>
	void DeleteChilds(GameObject obj) {
		List<GameObject> childs = new List<GameObject>();
		foreach (Transform child in obj.transform) {
			childs.Add(child.gameObject);
		}
		foreach (GameObject child in childs) {
			Destroy(child);
		}
	}

    /// <summary>
    /// Load savegames
    /// </summary>
	void LoadSaveGames() {
		// Delete childs
		DeleteChilds(pnl_save_games);
		// Load
		GameObject btn = null;
		List<SaveGame> saves = SaveGame.All();
		List<GameObject> objects = new List<GameObject>();
		foreach (SaveGame save in saves) {
			if (save.ID > 0) {
				btn = Instantiate<GameObject>(btn_save_game);
				btn.transform.SetParent(pnl_save_games.transform, false);
				btn_save_game savegame = btn.GetComponent<btn_save_game>();
				savegame.lbl_name.text = save.Name;
				savegame.ShowHearts(save.Hearts);
				objects.Add(btn);
			} else {
				btn = Instantiate<GameObject>(btn_new_game);
				btn.transform.SetParent(pnl_save_games.transform, false);
				objects.Add(btn);
			}
		}
		// Menu elements
		List<menu_navigation_element> navs = new List<menu_navigation_element>();
		for (int i = 0; i <= 2; i++) {
			navs.Add(objects[i].GetComponent<menu_navigation_element>());
		}
		navs[0].highlight_frame = frame_highlight;
		navs[0].select_up = objects[2];
		navs[0].select_down = objects[1];
		navs[0].InputStrength = 0.5f;
		navs[0].InputWait = 0.25f;
		navs[1].highlight_frame = frame_highlight;
		navs[1].select_up = objects[0];
		navs[1].select_down = objects[2];
		navs[1].InputStrength = 0.5f;
		navs[1].InputWait = 0.25f;
		navs[2].highlight_frame = frame_highlight;
		navs[2].select_up = objects[1];
		navs[2].select_down = objects[0];
		navs[2].InputStrength = 0.5f;
		navs[2].InputWait = 0.25f;
		// Buttons
		Button button = objects[0].GetComponent<Button>();
		button.onClick.AddListener(delegate{LoadSavegame(1);});
		button = objects[1].GetComponent<Button>();
		button.onClick.AddListener(delegate{LoadSavegame(2);});
		button = objects[2].GetComponent<Button>();
		button.onClick.AddListener(delegate{LoadSavegame(3);});
	}

    /// <summary>
    /// Select first element
    /// </summary>
    /// <returns></returns>
	IEnumerator SelectFirstElement() {
        yield return new WaitForFixedUpdate();
		EventSystem es = FindObjectOfType<EventSystem>();
		es.SetSelectedGameObject(FirstObject());
	}

    /// <summary>
    /// Press savegame
    /// </summary>
    /// <param name="ID"></param>
	public void LoadSavegame(int ID) {
		SelectedID = ID;
		SaveGame save = SaveGame.Load(ID);
		LoadedSaveGame = save;
		if (save.ID == 0) {
			mm.Navigate(menu_main.menu_main_state.NewGame, menu_main.menu_sound.Click);
		} else {
			mm.Navigate(menu_main.menu_main_state.LoadGame, menu_main.menu_sound.Click);
		}
	}

    /// <summary>
    /// Go back
    /// </summary>
	void Back() {
		mm.Navigate(menu_main.menu_main_state.PressA, menu_main.menu_sound.Cancel);
	}

}
