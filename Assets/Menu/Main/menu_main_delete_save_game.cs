﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class menu_main_delete_save_game : MonoBehaviour {

	public GameObject SelectFirst;
	public menu_main_savegame mms;
	private menu_main mm;
	
	void Start() {
		mm = FindObjectOfType<menu_main>();
	}
	
    void OnEnable()
    {
        UpdateSaveGame();
        StartCoroutine(SelectFirstElement());
    }

	void Update() {
		if (Controls.Control.GetBDown()) Cancel();
	}

    /// <summary>
    /// Update selected savegame
    /// </summary>
	void UpdateSaveGame() {
		btn_save_game savegame = FindObjectOfType<btn_save_game>();
		savegame.lbl_name.text = mms.LoadedSaveGame.Name;
		savegame.ShowHearts(mms.LoadedSaveGame.Hearts);
	}
	
    /// <summary>
    /// Select first element
    /// </summary>
    /// <returns></returns>
	IEnumerator SelectFirstElement() {
        yield return new WaitForFixedUpdate();
		EventSystem es = FindObjectOfType<EventSystem>();
		es.SetSelectedGameObject(SelectFirst);
	}
	
    /// <summary>
    /// Press delete
    /// </summary>
	public void Delete() {
		mms.LoadedSaveGame.Delete();
		mm.Navigate(menu_main.menu_main_state.SaveGame, menu_main.menu_sound.Cancel);
	}
	
    /// <summary>
    /// Go back / Press cancel
    /// </summary>
	public void Cancel() {
		mm.Navigate(menu_main.menu_main_state.LoadGame, menu_main.menu_sound.Cancel);
	}

}
