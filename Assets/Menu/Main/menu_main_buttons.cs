﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_main_buttons : MonoBehaviour {

	public GameObject SelectFirst;
	private menu_main mm;
	private bool initialized;

	void Start() {
		mm = FindObjectOfType<menu_main>();
	}

    void OnEnable()
    {
        StartCoroutine(SelectFirstElement());
    }

	void Update() {
		if (Controls.Control.GetBDown()) Back();
	}


    /// <summary>
    /// Select first element
    /// </summary>
    /// <returns></returns>
    IEnumerator SelectFirstElement()
    {
        EventSystem es = FindObjectOfType<EventSystem>();
        yield return new WaitForFixedUpdate();        
        es.SetSelectedGameObject(SelectFirst);
    }

    /// <summary>
    /// Press play
    /// </summary>
    public void Play() {
		mm.Navigate(menu_main.menu_main_state.SaveGame, menu_main.menu_sound.Click);
	}

    /// <summary>
    /// Press options
    /// </summary>
	public void Options() {
		mm.Navigate(menu_main.menu_main_state.Options, menu_main.menu_sound.Click);
	}

    /// <summary>
    /// Press exit
    /// </summary>
	public void Exit() {
		StartCoroutine(DoExit());
	}

	IEnumerator DoExit() {
		AudioSource.PlayClipAtPoint(Sounds.MenuClick, Camera.main.transform.position, Settings.SoundVolume);
		yield return new WaitForSeconds(Sounds.MenuClick.length);
		Application.Quit();
	}

    /// <summary>
    /// Go back
    /// </summary>
	void Back() {
		mm.Navigate(menu_main.menu_main_state.PressA, menu_main.menu_sound.Cancel);
	}

}
