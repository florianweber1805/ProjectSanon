﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using Helper;

public class menu_navigation_element : MonoBehaviour, ISelectHandler, IDeselectHandler {

	public float InputStrength = 0.5f;
	public float InputWait = 0.25f;
	public GameObject highlight_frame;
	public GameObject select_up;
	public GameObject select_down;
	public GameObject select_left;
	public GameObject select_right;
	
	private bool selected;
	private float initial_wait;

	void OnEnable() {
		selected = false;
	}

	void Update() {
		if (initial_wait <= 0f) {
			if (selected) Navigate();
		} else initial_wait -= Time.deltaTime;
	}

	void Navigate() {
		if (Controls.Control.GetLV() > InputStrength) {
			StartCoroutine(Select(select_up));
		} else if (Controls.Control.GetLV() < -InputStrength) {
			StartCoroutine(Select(select_down));
		} else if (Controls.Control.GetLH() > InputStrength) {
			StartCoroutine(Select(select_right));
		} else if (Controls.Control.GetLH() < -InputStrength) {
			StartCoroutine(Select(select_left));
		}
	}

	IEnumerator Select(GameObject ToSelect) {
		if (ToSelect) {
			EventSystem es = FindObjectOfType<EventSystem>();
			es.SetSelectedGameObject(ToSelect);
			AudioSource.PlayClipAtPoint(Sounds.MenuNavigate, Camera.main.transform.position, Settings.SoundVolume);
			yield return new WaitForSeconds(InputWait);
		}
	}

	public void OnSelect(BaseEventData eventData) {
		RectTransform frame_rec = highlight_frame.GetComponent<RectTransform>();
		RectTransform rec = gameObject.GetComponent<RectTransform>();
		frame_rec.position = rec.position;
		frame_rec.sizeDelta = rec.sizeDelta;
		initial_wait = InputWait;
		selected = true;
	}

	public void OnDeselect(BaseEventData eventData) {
		selected = false;
	}

}
