﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menu_wiggle : MonoBehaviour {

	public bool Active;
	public Vector2 MaxOffset = new Vector2(10f, 10f);
	public Vector2 Step = new Vector2(0.5f, 0.5f);

	private RectTransform rec;
	private Vector3 orig_pos;
	private bool horizontal_direction = true;
	private bool vertical_direction = true;

	// Use this for initialization
	void Start () {
		rec = GetComponent<RectTransform>();
		orig_pos = rec.localPosition;
		rec.localPosition = new Vector3(orig_pos.x + Random.Range(-MaxOffset.x, MaxOffset.x), 
		                                orig_pos.y + Random.Range(-MaxOffset.y, MaxOffset.y), 0f);		
	}
	
	// Update is called once per frame
	void Update () {
		if (Active) {
			Vector3 new_pos = rec.localPosition;
			// Horizontal
			if (horizontal_direction) {
				if (new_pos.x < orig_pos.x + MaxOffset.x) {
					new_pos.x += Step.x * Time.deltaTime;
				} else {
					horizontal_direction = !horizontal_direction;
				}
			} else {
				if (new_pos.x > orig_pos.x - MaxOffset.x) {
					new_pos.x -= Step.x * Time.deltaTime;
				} else {
					horizontal_direction = !horizontal_direction;
				}
			}
			new_pos.x = Mathf.Clamp(new_pos.x, orig_pos.x - MaxOffset.x, orig_pos.x + MaxOffset.x);
			// Vertical
			if (vertical_direction) {
				if (new_pos.y < orig_pos.y + MaxOffset.y) {
					new_pos.y += Step.y * Time.deltaTime;
				} else {
					vertical_direction = !vertical_direction;
				}
			} else {
				if (new_pos.y > orig_pos.y - MaxOffset.y) {
					new_pos.y -= Step.y * Time.deltaTime;
				} else {
					vertical_direction = !vertical_direction;
				}
			}
			new_pos.y = Mathf.Clamp(new_pos.y, orig_pos.y - MaxOffset.y, orig_pos.y + MaxOffset.y);
			// Set position
			rec.localPosition = new_pos;
		}
	}

}
