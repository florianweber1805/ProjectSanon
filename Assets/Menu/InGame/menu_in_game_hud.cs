﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menu_in_game_hud : MonoBehaviour {

    public enum ActionButton { A, X };
    public enum Action { None, Roll, Draw, Sheath, Attack, Speak, Dodge, Jump };

    [SerializeField]
    private GameObject a;
    [SerializeField]
    private GameObject b;
    [SerializeField]
    private GameObject x;
    [SerializeField]
    private GameObject y;
    [SerializeField]
    private GameObject r;
    [SerializeField]
    private GameObject r2;
    [SerializeField]
    private Text txt_a;
    [SerializeField]
    private Text txt_x;

    private Action a_action;
    private Action x_action;

    /// <summary>
    /// Set specific action for specific button
    /// </summary>
    /// <param name="Button"></param>
    /// <param name="Action"></param>
    public void SetAction(ActionButton Button, Action Action)
    {
        // Button
        Text txt = null;
        switch (Button)
        {
            case ActionButton.A:
                txt = txt_a;
                if (a_action == Action)
                {                    
                    return;
                }
                a_action = Action;
                break;
            case ActionButton.X:
                txt = txt_x;
                if (x_action == Action)
                {                    
                    return;
                }
                x_action = Action;
                break;
        }
        // Set text
        switch (Action)
        {
            case Action.Attack:
                txt.text = "Attack";
                break;
            case Action.Draw:
                txt.text = "Draw Sword";
                break;
            case Action.Roll:
                txt.text = "Roll";
                break;
            case Action.Sheath:
                txt.text = "Put Away";
                break;
            case Action.None:
                txt.text = "";
                break;
            case Action.Speak:
                txt.text = "Speak";
                break;
            case Action.Dodge:
                txt.text = "Dodge";
                break;
            case Action.Jump:
                txt.text = "Jump";
                break;
        }
    }

    /// <summary>
    /// Hide / show buttons
    /// </summary>
    /// <param name="Hide"></param>
    public void HideButtons(bool Hide)
    {
        a.SetActive(!Hide);
        b.SetActive(!Hide);
        x.SetActive(!Hide);
        y.SetActive(!Hide);
    }

    public void HideActionButtons(bool Hide)
    {
        a.SetActive(!Hide);
        x.SetActive(!Hide);
        r.SetActive(!Hide);
        r2.SetActive(!Hide);
    }

    /// <summary>
    /// View icon
    /// </summary>
    /// <param name="FirstPerson"></param>
    public void SwitchView(bool FirstPerson)
    {
        r.SetActive(!FirstPerson);
        r2.SetActive(FirstPerson);
    }

}
