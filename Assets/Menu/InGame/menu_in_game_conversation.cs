﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Helper;

public class menu_in_game_conversation : MonoBehaviour {

    private enum FontSizes { Normal = 60, Small = 30, Big = 120 };

    // Objects
    [SerializeField]
    private Text txt;
    [SerializeField]
    private GameObject button;
    private PlayerCamera cam;
    private menu_in_game m;

    private bool finished;
    private List<ConversationPiece> ConversationPieces;
    private int index;

    //void OnEnable()
    //{
    //    Pausing.Pause(Pausing.PauseType.Game);
    //}

    /// <summary>
    /// Start
    /// </summary>
    void Start()
    {
        m = FindObjectOfType<menu_in_game>();
        cam = FindObjectOfType<PlayerCamera>();
    }

    /// <summary>
    /// Open a conversation
    /// </summary>
    /// <param name="Conversation"></param>
    public void Open(Conversation Conversation)
    {        
        ConversationPieces = Conversation.Speak();
        index = 0;
        Post(ConversationPieces[index]);
    }

    /// <summary>
    /// Main update cycle
    /// </summary>
    void Update()
    {
        if (finished)
        {
            if (Controls.Control.GetADown())
            {
                //cam.CameraState = CameraState.Normal;
                if (index < ConversationPieces.Count - 1)
                {
                    AudioSource.PlayClipAtPoint(Sounds.ConversationNext, Camera.main.transform.position, Settings.SoundVolume);
                    index++;
                    Post(ConversationPieces[index]);
                }
                else
                {
                    AudioSource.PlayClipAtPoint(Sounds.ConversationFinish, Camera.main.transform.position, Settings.SoundVolume);
                    //Pausing.Resume(Pausing.PauseType.Game);
                    cam.ToggleSpeaking();
                    m.Show(menu_in_game.MenuPart.HUD);
                }                
            }
        }
    }

    /// <summary>
    /// Post message
    /// </summary>
    /// <param name="ConversationPiece"></param>
	void Post(ConversationPiece ConversationPiece)
    {
        finished = false;
        button.SetActive(false);
        txt.text = string.Empty;
        StartCoroutine(post(ConversationPiece));
    }

    /// <summary>
    /// Post message
    /// </summary>
    /// <param name="ConversationPiece"></param>
    /// <returns></returns>
    IEnumerator post(ConversationPiece ConversationPiece)
    {
        // Text Size
        switch (ConversationPiece.Size)
        {
            case ConversationPiece.ConversationFontSize.Normal:
                txt.resizeTextMaxSize = (int)FontSizes.Normal;
                break;
            case ConversationPiece.ConversationFontSize.Big:
                txt.resizeTextMaxSize = (int)FontSizes.Big;
                break;
            case ConversationPiece.ConversationFontSize.Small:
                txt.resizeTextMaxSize = (int)FontSizes.Small;
                break;
        }
        // Text
        foreach (char c in ConversationPiece.Text)
        {
            txt.text += c;
            yield return new WaitForSeconds(Settings.TextSpeed * ConversationPiece.Speed);
        }
        button.SetActive(true);               
        finished = true;
    }

}
