﻿using UnityEngine;
using System.Collections;

public class menu_in_game_equipment : MonoBehaviour {

    [SerializeField]
    private menu_in_game m;

    /// <summary>
    /// Main update cycle
    /// </summary>
    void Update()
    {
        // Switch page
        if (Controls.Control.GetRBDown())
        {
            m.Show(menu_in_game.MenuPart.Status);
        }
        else if (Controls.Control.GetLBDown())
        {
            m.Show(menu_in_game.MenuPart.Items);
        }
        // Exit
        if (Controls.Control.GetStartDown())
        {
            m.Show(menu_in_game.MenuPart.HUD);
        }
    }

}
