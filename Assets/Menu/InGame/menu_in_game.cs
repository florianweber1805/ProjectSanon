﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menu_in_game : MonoBehaviour {

    public enum MenuPart { HUD, Conversation, Options, Items, Status, Map, Equipment };

    // Objects
    [SerializeField]
    private GameObject HUD;
    [SerializeField]
    private GameObject Conversation;
    [SerializeField]
    private GameObject Options;
    [SerializeField]
    private GameObject Items;
    [SerializeField]
    private GameObject Status;
    [SerializeField]
    private GameObject Equipment;
    [SerializeField]
    private GameObject MenuHUD;

    private menu_in_game_hud hud;

    /// <summary>
    /// Awake
    /// </summary>
    void Awake()
    {
        hud = HUD.GetComponent<menu_in_game_hud>();
    }

    /// <summary>
    /// Enable
    /// </summary>
    void OnEnable()
    {
        Show(MenuPart.HUD);
    }

    /// <summary>
    /// Show specific menu panel
    /// </summary>
    /// <param name="Menu"></param>
	public void Show(MenuPart Menu)
    {
        // Hide all
        //HUD.SetActive(false);
        hud.HideButtons(false);
        Conversation.SetActive(false);
        Options.SetActive(false);
        Items.SetActive(false);
        Status.SetActive(false);
        Equipment.SetActive(false);
        MenuHUD.SetActive(false);
        // Show
        switch (Menu)
        {
            case MenuPart.HUD:
                StartCoroutine(CloseMenu());
                break;
            case MenuPart.Conversation:
                Conversation.SetActive(true);
                HUD.SetActive(false);
                if (!Pausing.Paused()) Pausing.Pause();
                break;
            case MenuPart.Options:
                Options.SetActive(true);
                HUD.SetActive(false);
                if (!Pausing.Paused()) Pausing.Pause();
                break;
            case MenuPart.Items:
                Items.SetActive(true);
                ShowMenuHUD();
                if (!Pausing.Paused()) Pausing.Pause();
                break;
            case MenuPart.Status:
                Status.SetActive(true);
                ShowMenuHUD();
                if (!Pausing.Paused()) Pausing.Pause();
                break;
            case MenuPart.Equipment:
                Equipment.SetActive(true);
                ShowMenuHUD();
                break;
        }
    }

    void ShowMenuHUD()
    {
        MenuHUD.SetActive(true);
        HUD.SetActive(true);
        hud.HideActionButtons(true);
    }

    IEnumerator CloseMenu()
    {
        yield return new WaitForEndOfFrame();
        HUD.SetActive(true);
        if (Pausing.Paused()) Pausing.Resume();
    }

}
